package asmaki.delivery.upbeat.digital.ui.home.changepassword;

import asmaki.delivery.upbeat.digital.data.DataManager;
import asmaki.delivery.upbeat.digital.data.model.ApiRoot;
import asmaki.delivery.upbeat.digital.ui.base.BaseNavigator;
import asmaki.delivery.upbeat.digital.ui.base.BaseViewModel;
import asmaki.delivery.upbeat.digital.utils.AppConstants;
import asmaki.delivery.upbeat.digital.utils.ResourceProvider;
import asmaki.delivery.upbeat.digital.utils.rx.SchedulerProvider;

import static asmaki.delivery.upbeat.digital.utils.LogUtils.LOGD;
import static asmaki.delivery.upbeat.digital.utils.LogUtils.LOGE;

public class ChangePasswordViewModel extends BaseViewModel<BaseNavigator.ShowAlert> {
    public ChangePasswordViewModel(DataManager dataManager, SchedulerProvider schedulerProvider, ResourceProvider mResourceProvider) {
        super(dataManager, schedulerProvider, mResourceProvider);
    }



    String getPassword = getDataManager().getPwd();


    public void changePassword(String OldPassword,String NewPassword,String ConfirmPAssword) {

        getCompositeDisposable().add(getDataManager().changePassword(OldPassword, NewPassword, ConfirmPAssword)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .doOnSubscribe(s -> setIsLoading(true))
                .doAfterTerminate(() -> setIsLoading(false))
                .subscribe(
                        defaultResponce -> {
                            if (defaultResponce.getmStatusCode().equals(String.valueOf(ApiRoot.SUCCESS))) {
                                LOGD("SUCCESS");
                                getDataManager().setPwd(NewPassword);
                                getNavigator().showAlertDialog(defaultResponce.getmMessage(), AppConstants.SUCCESS_MESSAGE);

                            } else {
                                LOGE("ERROR");
                                getNavigator().showAlertDialog(defaultResponce.getmMessage(), AppConstants.ERROR_MESSAGE);
                            }
                        }, throwable -> {
                            LOGE("ERROR:" + throwable.getMessage());
                            getNavigator().showAlertDialog(throwable.getMessage(), AppConstants.NOCONNECTION);

                        }
                ));
        //    }
    }

    String getLanguage(){

        return getDataManager().getLang();
    }
}

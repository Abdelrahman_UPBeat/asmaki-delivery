package asmaki.delivery.upbeat.digital.ui.home.notification;

import asmaki.delivery.upbeat.digital.data.DataManager;
import asmaki.delivery.upbeat.digital.data.model.ApiRoot;
import asmaki.delivery.upbeat.digital.ui.base.BaseViewModel;
import asmaki.delivery.upbeat.digital.utils.AppConstants;
import asmaki.delivery.upbeat.digital.utils.ResourceProvider;
import asmaki.delivery.upbeat.digital.utils.rx.SchedulerProvider;

import static asmaki.delivery.upbeat.digital.utils.LogUtils.LOGD;
import static asmaki.delivery.upbeat.digital.utils.LogUtils.LOGE;

public class NotificationViewModel extends BaseViewModel<NotificationNavigator> {
    public NotificationViewModel(DataManager dataManager, SchedulerProvider schedulerProvider, ResourceProvider mResourceProvider) {
        super(dataManager, schedulerProvider, mResourceProvider);
        getNotf();
    }


    public void getNotf() {

        getCompositeDisposable().add(getDataManager().getNotf()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .doOnSubscribe(s -> setIsLoading(true))
                .doAfterTerminate(() -> setIsLoading(false))
                .subscribe(
                        myNotification -> {
                            if (myNotification.getStatusCode().equals(String.valueOf(ApiRoot.SUCCESS))) {
                                LOGD("SUCCESS");
                                getNavigator().getNotification(myNotification.getmData());

                            } else {
                                LOGE("ERROR");
                            }
                        }, throwable -> {
                            LOGE("ERROR:" + throwable.getMessage());
                            getNavigator().showAlertDialog(throwable.getMessage(), AppConstants.NOCONNECTION);

                        }
                ));
        //    }
    }
}

package asmaki.delivery.upbeat.digital.ui.home.rates;

import java.util.ArrayList;

import asmaki.delivery.upbeat.digital.data.model.MyOrder;
import asmaki.delivery.upbeat.digital.ui.base.BaseNavigator;

public interface RatesNavigator extends BaseNavigator.ShowAlert {

    void getMyOrders(ArrayList<MyOrder.Data> myOrders);

}

package asmaki.delivery.upbeat.digital.data.remote.retrofitrepo;

import java.util.HashMap;

import asmaki.delivery.upbeat.digital.data.model.AboutResponce;
import asmaki.delivery.upbeat.digital.data.model.Addresses;
import asmaki.delivery.upbeat.digital.data.model.CallUsResponce;
import asmaki.delivery.upbeat.digital.data.model.CategoryResponse;
import asmaki.delivery.upbeat.digital.data.model.City;
import asmaki.delivery.upbeat.digital.data.model.DefaultResponce;
import asmaki.delivery.upbeat.digital.data.model.LogoutResponce;
import asmaki.delivery.upbeat.digital.data.model.MyNotification;
import asmaki.delivery.upbeat.digital.data.model.MyOrder;
import asmaki.delivery.upbeat.digital.data.model.Product;
import asmaki.delivery.upbeat.digital.data.model.Rate;
import asmaki.delivery.upbeat.digital.data.model.SendPaymentRequest;
import asmaki.delivery.upbeat.digital.data.model.Size;
import asmaki.delivery.upbeat.digital.data.model.SliderResponce;
import asmaki.delivery.upbeat.digital.data.model.TermsResponce;
import asmaki.delivery.upbeat.digital.data.model.User;
import asmaki.delivery.upbeat.digital.data.remote.ApiEndPoint;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiRequests {


  @POST(ApiEndPoint.REGISTER)
  Single<User> register(@Query(ApiConstants.FIRST_NAME) String first_name,
                        @Query(ApiConstants.LAST_NAME) String last_name,
                        @Query(ApiConstants.EMAIL) String email,
                        @Query(ApiConstants.PWD) String password,
                        @Query(ApiConstants.CONFIRM_PASSWORD) String confirm_password,
                        @Query(ApiConstants.phone_number) String phone);


  @POST(ApiEndPoint.LOGIN)
  Single<User> Login(@Query(ApiConstants.EMAIL) String email,
                     @Query(ApiConstants.PWD) String password,
                     @Query("firebase_token") String firebase_token);

  @POST(ApiEndPoint.FORGETPASSWORD)
  Single<DefaultResponce> forgetPassword(@Query(ApiConstants.EMAIL) String email);

  @GET(ApiEndPoint.CITIES)
  Single<City>getCities();

  @GET(ApiEndPoint.ABOUT)
  Single<AboutResponce>getAbout();

  @GET(ApiEndPoint.PRIVACY)
  Single<TermsResponce>getPrivacy();

  @POST(ApiEndPoint.RATE)
  Single<Rate>setRateAPI(@Query(ApiConstants.RATE) String rate);

  @POST(ApiEndPoint.CONTACTUS)
  Single<CallUsResponce>callUS(@Query(ApiConstants.NAME) String name, @Query(ApiConstants.EMAIL) String email,
                               @Query(ApiConstants.PHONE_NUMBER) String phone, @Query(ApiConstants.MESSAGE) String message);

  @POST(ApiEndPoint.LOGOUT)
  Single<LogoutResponce>logOutApi();


  @POST(ApiEndPoint.CHANGEPASSWORD)
  Single<DefaultResponce>changePassword(@Query(ApiConstants.OLD_PASSWORD) String oldPassword, @Query(ApiConstants.NEWPASSWORD) String newPassword,
                                        @Query(ApiConstants.CONFIRMPASSWORD) String confirmPassword);

  @GET(ApiEndPoint.CLIENTADDRESS)
  Single<Addresses>getMyAddress();

  @POST(ApiEndPoint.ADDADRESS)
  Single<DefaultResponce>addAddress(@Query(ApiConstants.ADDRESSNAME) String ADDRESSNAME, @Query(ApiConstants.CITYID) String CITYID,
                                    @Query(ApiConstants.ADDRESS) String ADDRESS, @Query(ApiConstants.LATITUDE) String LATITUDE
          , @Query(ApiConstants.LONGITUDE) String LONGITUDE);

  @DELETE("clients/delete_address/" + "{delete_address}")
  Single<DefaultResponce> deleteAddress(@Path(value = "delete_address", encoded = true) String AddressID);

  @POST("clients/edit_address/" + "{address}")
  Single<DefaultResponce> updateAddress(@Path(value = "address", encoded = true) String AddressID, @Query(ApiConstants.ADDRESSNAME) String ADDRESSNAME,
                                        @Query(ApiConstants.CITYID) String CITYID,
                                        @Query(ApiConstants.ADDRESS) String ADDRESS, @Query(ApiConstants.LATITUDE) String LATITUDE,
                                        @Query(ApiConstants.LONGITUDE) String LONGITUDE);


  @POST(ApiEndPoint.EDITPROFILE)
  Single<User>editProfile(@Body HashMap<String, String> map);

  @GET(ApiEndPoint.CATEGORY)
  Single<CategoryResponse>getCategories();

  @GET(ApiEndPoint.PRODUCTS)
  Single<Product>getProducts(@Query(ApiConstants.CATEGORYID) String category_id);

  @GET(ApiEndPoint.SLIDER)
  Single<SliderResponce>getSlider();

  @GET(ApiEndPoint.PRODUCTS)
  Single<Product>getAllProducts();

  @GET(ApiEndPoint.SIZES)
  Single<Size>getAllSizes();


  @POST(ApiEndPoint.Chat)
  Single<DefaultResponce>setChatNotf(@Query(ApiConstants.node_child_id) String nodeChild, @Query(ApiConstants.message) String message,
                                     @Query("node_name") String node_name);



  @POST("clients/place_order")
  Single<DefaultResponce> placeOrder(@Query("address_id") String address_id,
                                     @Query("comment") String comment,
                                     @Body SendPaymentRequest sendPaymentRequest,
                                     @Query("payment_method") String payment_method);


  @GET(ApiEndPoint.MYORDERS)
  Single<MyOrder>getAllOrders();


  @POST("deliveries/status")
  Single<DefaultResponce>changeStatus();

    @POST("deliveries/finish_order")
    Single<DefaultResponce>finish_order(@Query("order_id") String OrderID);



  @POST("deliveries/notifi")
  Single<MyNotification>getNotf();


}

package asmaki.delivery.upbeat.digital.ui.home.chat;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Map;

import javax.inject.Inject;

import asmaki.delivery.upbeat.digital.R;
import asmaki.delivery.upbeat.digital.constants.ViewModelProviderFactory;
import asmaki.delivery.upbeat.digital.data.model.Message;
import asmaki.delivery.upbeat.digital.databinding.FragmentChatBinding;
import asmaki.delivery.upbeat.digital.ui.adapter.ChatAdapter;
import asmaki.delivery.upbeat.digital.ui.base.BaseFragment;
import asmaki.delivery.upbeat.digital.ui.base.BaseNavigator;


public class ChatFragment extends BaseFragment<FragmentChatBinding, ChatViewModel> implements BaseNavigator.ShowAlert {


    @Inject
    ViewModelProviderFactory factory;
    private ChatViewModel chatViewModel;
    private FragmentChatBinding mBinding;

    FirebaseDatabase database;
    String UserID, UserImage;

    ChatAdapter chatAdapter;

    boolean loged, firstTimeHere = false;

    @Override
    public int getBindingVariable() {
        return asmaki.delivery.upbeat.digital.BR.viewModel;

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_chat;
    }

    @Override
    public ChatViewModel getViewModel() {
        chatViewModel = ViewModelProviders.of(this, factory).get(ChatViewModel.class);
        return chatViewModel;
    }


    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding = getViewDataBinding();
        chatViewModel.setNavigator(this);



        RelativeLayout progressBar = (RelativeLayout) getActivity().findViewById(R.id.helperLin);
        progressBar.setVisibility(View.GONE);





        UserID = chatViewModel.getUserID();
        UserImage = chatViewModel.getUserImage();
        loged = chatViewModel.checkLoged();

        String AutoRepley = getResources().getString(R.string.auto_repley);

        mBinding.btFire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String Message = mBinding.chatMessage.getText().toString();

                if (!Message.equals("")) {
                    if (loged == true) {
                        chatViewModel.sendMessage(Message);
                        if (firstTimeHere == false) {

                            chatViewModel.AutoRepleyLoged(AutoRepley);
                            chatViewModel.setChatNotf(Message);
                            firstTimeHere = true;
                        }

                    }
                    mBinding.chatMessage.setText("");
                }
            }

        });

        if (loged == true) {

            try {

//                mBinding.spinKit.setVisibility(View.GONE);
//                mBinding.chtHolder.setVisibility(View.GONE);
                database = FirebaseDatabase.getInstance();


                final ArrayList<Message> Messages = new ArrayList<>();

                DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference().child("Delivery/" + UserID);
                rootRef.addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                        Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                        Message message = new Message();
                        String MyMessage = map.get("Message").toString();
                        String date = map.get("CreatedTime").toString();
                        String Sender = map.get("sender").toString();
                        message.setMyMessage(MyMessage);
                        message.setSender(Sender);
                        message.setDate(date);
                        Messages.add(message);


                        generateCharities(Messages, UserImage);

                        if (Sender.equals("mobile")) {


                        } else {


                        }


//                CustUsersChat customList = new CustUsersChat(Chat.this, texts, user, datess, userPhoto,voices);
//                ChatMssage.setAdapter(customList);


                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }


                });
            } catch (Exception e) {


            }
        } else {


        }

        mBinding.backbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().onBackPressed();

            }
        });


        if(chatViewModel.getLanguage().equals("ar")){

            mBinding.backbt.setRotation(0);
            mBinding.btFire.setRotation(0);

        }


    }


    private void generateCharities(ArrayList<Message> messages, String UserImage) {

        String Lang = chatViewModel.getOurLang();

        if (getContext() != null)
            chatAdapter = new ChatAdapter(getContext(), messages, UserImage,Lang);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());

        mBinding.resChat.setLayoutManager(layoutManager);

        mBinding.resChat.setAdapter(chatAdapter);
        mBinding.resChat.scrollToPosition(messages.size() - 1);
    }

    @Override
    public void showAlertDialog(String message, String message_type) {

    }


}
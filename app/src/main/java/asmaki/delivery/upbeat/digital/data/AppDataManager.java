package asmaki.delivery.upbeat.digital.data;


import android.content.Context;

import java.util.HashMap;

import javax.inject.Inject;
import javax.inject.Singleton;

import asmaki.delivery.upbeat.digital.data.local.prefs.PreferencesHelper;
import asmaki.delivery.upbeat.digital.data.model.AboutResponce;
import asmaki.delivery.upbeat.digital.data.model.Addresses;
import asmaki.delivery.upbeat.digital.data.model.CallUsResponce;
import asmaki.delivery.upbeat.digital.data.model.CategoryResponse;
import asmaki.delivery.upbeat.digital.data.model.City;
import asmaki.delivery.upbeat.digital.data.model.DefaultResponce;
import asmaki.delivery.upbeat.digital.data.model.LogoutResponce;
import asmaki.delivery.upbeat.digital.data.model.MyNotification;
import asmaki.delivery.upbeat.digital.data.model.MyOrder;
import asmaki.delivery.upbeat.digital.data.model.Product;
import asmaki.delivery.upbeat.digital.data.model.Rate;
import asmaki.delivery.upbeat.digital.data.model.SendPaymentRequest;
import asmaki.delivery.upbeat.digital.data.model.Size;
import asmaki.delivery.upbeat.digital.data.model.SliderResponce;
import asmaki.delivery.upbeat.digital.data.model.TermsResponce;
import asmaki.delivery.upbeat.digital.data.model.User;
import asmaki.delivery.upbeat.digital.data.remote.ApiHelper;
import io.reactivex.Single;

@Singleton
public class AppDataManager implements DataManager{
    private final PreferencesHelper mPreferencesHelper;
    private final ApiHelper mApiHelper;
    private final Context mContext;

    @Inject
    public AppDataManager(Context context, PreferencesHelper mPreferencesHelper,
                          ApiHelper mApiHelper) {
        this.mPreferencesHelper = mPreferencesHelper;
        this.mApiHelper = mApiHelper;
        this.mContext = context;
    }

    @Override
    public void setDeviceID(String device_id) {
        mPreferencesHelper.setDeviceID(device_id);
    }

    @Override
    public String getDeviceID() {
        return mPreferencesHelper.getDeviceID();
    }

    @Override
    public void setOnBoardingVisible(boolean isVisible) {
        mPreferencesHelper.setOnBoardingVisible(isVisible);
    }

    @Override
    public boolean getOnBoardingVisible() {
        return mPreferencesHelper.getOnBoardingVisible();
    }

    @Override
    public void setIsLangSelected(boolean isLangSelected) {
        mPreferencesHelper.setIsLangSelected(isLangSelected);
    }

    @Override
    public boolean getIsLangSelected() {
        return mPreferencesHelper.getIsLangSelected();
    }

    @Override
    public void setLang(String lang) {
        mPreferencesHelper.setLang(lang);
    }

    @Override
    public String getLang() {
        return mPreferencesHelper.getLang();
    }

    @Override
    public int getCurrentUserLoggedInMode() {
        return mPreferencesHelper.getCurrentUserLoggedInMode();
    }

    @Override
    public void setCurrentUserLoggedInMode(LoggedInMode mode) {
        mPreferencesHelper.setCurrentUserLoggedInMode(mode);
    }

    @Override
    public void setToken(String token) {
        mPreferencesHelper.setToken(token);
    }

    @Override
    public String getToken() {
        return mPreferencesHelper.getToken();
    }

    @Override
    public void setUserType(String type) {
        mPreferencesHelper.setUserType(type);
    }

    @Override
    public String getUserType() {
        return mPreferencesHelper.getUserType();
    }

    @Override
    public void logOut() {
        mPreferencesHelper.logOut();
    }

    @Override
    public void setPhone(String phone) {
        mPreferencesHelper.setPhone(phone);
    }

    @Override
    public String getPhone() {
        return mPreferencesHelper.getPhone();
    }

    @Override
    public void setPwd(String pwd) {
        mPreferencesHelper.setPwd(pwd);
    }

    @Override
    public String getPwd() {
        return mPreferencesHelper.getPwd();
    }

    @Override
    public void setIsRememberMe(boolean isRememberMe) {
        mPreferencesHelper.setIsRememberMe(isRememberMe);
    }

    @Override
    public boolean getIsRememberMe() {
        return mPreferencesHelper.getIsRememberMe();
    }

    @Override
    public void setImage(String image) {
        mPreferencesHelper.setImage(image);
    }

    @Override
    public String getImage() {
        return mPreferencesHelper.getImage();
    }

    @Override
    public void setEmail(String email) {
        mPreferencesHelper.setEmail(email);
    }

    @Override
    public String getEmail() {
        return mPreferencesHelper.getEmail();
    }

    @Override
    public void setName(String name) {
        mPreferencesHelper.setName(name);
    }

    @Override
    public String getName() {
        return mPreferencesHelper.getName();
    }

    @Override
    public void setPhoneNumber(String phoneNumber) {
        mPreferencesHelper.setPhoneNumber(phoneNumber);
    }

    @Override
    public String getPhoneNumber() {
        return mPreferencesHelper.getPhoneNumber();
    }

    @Override
    public void setCountryText(String countryText) {
        mPreferencesHelper.setCountryText(countryText);
    }

    @Override
    public String getCountryText() {
        return mPreferencesHelper.getCountryText();
    }

    @Override
    public void setCityText(String cityText) {
        mPreferencesHelper.setCityText(cityText);
    }

    @Override
    public String getCityText() {
        return mPreferencesHelper.getCityText();
    }

    @Override
    public void setCountryId(String countryId) {
        mPreferencesHelper.setCountryId(countryId);
    }

    @Override
    public String getCountryId() {
        return mPreferencesHelper.getCountryId();
    }

    @Override
    public void setCityId(String cityId) {
        mPreferencesHelper.setCityId(cityId);
    }

    @Override
    public String getCityId() {
        return mPreferencesHelper.getCityId();
    }

    @Override
    public void setUserId(String userId) {
        mPreferencesHelper.setUserId(userId);
    }

    @Override
    public String getUserId() {
        return mPreferencesHelper.getUserId();
    }

    @Override
    public void setValid(String valid) {
        mPreferencesHelper.setValid(valid);
    }

    @Override
    public String getValid() {
        return mPreferencesHelper.getValid();
    }

    @Override
    public void setAvailable(String valid) {
        mPreferencesHelper.setAvailable(valid);
    }

    @Override
    public String getAvailable() {
        return mPreferencesHelper.getAvailable();
    }

    @Override
    public void setMyRate(float rate) {
        mPreferencesHelper.setMyRate(rate);
    }

    @Override
    public String getRate() {
        return mPreferencesHelper.getRate();
    }


    @Override
    public Single<User> register(String first_name, String last_name, String email, String password, String confirm_password, String phone) {
        return mApiHelper.register(first_name, last_name, email, password, confirm_password, phone);
    }

    @Override
    public Single<User> Login(String email, String password,String firebase_token) {
        return mApiHelper.Login(email,password,firebase_token);
    }

    @Override
    public Single<DefaultResponce> forgetPassword(String email) {
        return mApiHelper.forgetPassword(email);
    }

    @Override
    public Single<City> getCities() {
        return mApiHelper.getCities();
    }

    @Override
    public Single<AboutResponce> getAbout() {
        return mApiHelper.getAbout();
    }

    @Override
    public Single<TermsResponce> getPrivacy() {
        return mApiHelper.getPrivacy();
    }

    @Override
    public Single<Rate> setRateAPI(String rate) {
        return mApiHelper.setRateAPI(rate);
    }

    @Override
    public Single<CallUsResponce> callUS(String name, String email, String phone, String message) {
        return mApiHelper.callUS(name, email, phone, message);
    }

    @Override
    public Single<LogoutResponce> logOutApi() {
        return mApiHelper.logOutApi();
    }

    @Override
    public Single<DefaultResponce> changePassword(String oldPassword, String newPassword, String confirmPassword) {
        return mApiHelper.changePassword(oldPassword, newPassword, confirmPassword);
    }

    @Override
    public Single<Addresses> getMyAddress() {
        return mApiHelper.getMyAddress();
    }

    @Override
    public Single<DefaultResponce> addAddress(String ADDRESSNAME, String CITYID, String ADDRESS, String LATITUDE, String LONGITUDE) {
        return mApiHelper.addAddress(ADDRESSNAME, CITYID, ADDRESS, LATITUDE, LONGITUDE);
    }

    @Override
    public Single<DefaultResponce> deleteAddress(String AddressID) {
        return mApiHelper.deleteAddress(AddressID);
    }

    @Override
    public Single<DefaultResponce> updateAddress(String AddressID, String ADDRESSNAME, String CITYID, String ADDRESS, String LATITUDE, String LONGITUDE) {
        return mApiHelper.updateAddress(AddressID, ADDRESSNAME, CITYID, ADDRESS, LATITUDE, LONGITUDE);
    }



    @Override
    public Single<User> editProfile(HashMap<String, String> map) {
        return mApiHelper.editProfile(map);
    }

    @Override
    public Single<CategoryResponse> getCategories() {
        return mApiHelper.getCategories();
    }

    @Override
    public Single<Product> getProducts(String category_id) {
        return mApiHelper.getProducts(category_id);
    }

    @Override
    public Single<SliderResponce> getSlider() {
        return mApiHelper.getSlider();
    }

    @Override
    public Single<Product> getAllProducts() {
        return mApiHelper.getAllProducts();
    }

    @Override
    public Single<Size> getAllSizes() {
        return mApiHelper.getAllSizes();
    }

    @Override
    public Single<DefaultResponce> setChatNotf(String nodeChild, String message,String node_name) {
        return mApiHelper.setChatNotf(nodeChild, message, node_name);
    }

    @Override
    public Single<DefaultResponce> placeOrder(String address_id, String comment, SendPaymentRequest sendPaymentRequest, String payment_method) {
        return mApiHelper.placeOrder(address_id, comment, sendPaymentRequest, payment_method);
    }

    @Override
    public Single<MyOrder> getAllOrders() {
        return mApiHelper.getAllOrders();
    }

    @Override
    public Single<DefaultResponce> changeStatus() {
        return mApiHelper.changeStatus();
    }

    @Override
    public Single<DefaultResponce> finish_order(String OrderID) {
        return mApiHelper.finish_order(OrderID);
    }

    @Override
    public Single<MyNotification> getNotf() {
        return mApiHelper.getNotf();
    }


}

package asmaki.delivery.upbeat.digital.data.remote;

import java.util.HashMap;

import asmaki.delivery.upbeat.digital.data.model.AboutResponce;
import asmaki.delivery.upbeat.digital.data.model.Addresses;
import asmaki.delivery.upbeat.digital.data.model.CallUsResponce;
import asmaki.delivery.upbeat.digital.data.model.CategoryResponse;
import asmaki.delivery.upbeat.digital.data.model.City;
import asmaki.delivery.upbeat.digital.data.model.DefaultResponce;
import asmaki.delivery.upbeat.digital.data.model.LogoutResponce;
import asmaki.delivery.upbeat.digital.data.model.MyNotification;
import asmaki.delivery.upbeat.digital.data.model.MyOrder;
import asmaki.delivery.upbeat.digital.data.model.Product;
import asmaki.delivery.upbeat.digital.data.model.Rate;
import asmaki.delivery.upbeat.digital.data.model.SendPaymentRequest;
import asmaki.delivery.upbeat.digital.data.model.Size;
import asmaki.delivery.upbeat.digital.data.model.SliderResponce;
import asmaki.delivery.upbeat.digital.data.model.TermsResponce;
import asmaki.delivery.upbeat.digital.data.model.User;
import io.reactivex.Single;
import retrofit2.http.Query;

public interface ApiHelper {

    Single<User> register(String first_name, String last_name, String email, String password, String confirm_password, String phone);
    Single<User> Login(String email, String password,String firebase_token);
    Single<DefaultResponce> forgetPassword(String email);
    Single<City> getCities();
    Single<AboutResponce>getAbout();
    Single<TermsResponce>getPrivacy();
    Single<Rate>setRateAPI(String rate);
    Single<CallUsResponce>callUS(String name, String email, String phone, String message);
    Single<LogoutResponce>logOutApi();
    Single<DefaultResponce>changePassword(String oldPassword, String newPassword, String confirmPassword);
    Single<Addresses>getMyAddress();
    Single<DefaultResponce>addAddress(String ADDRESSNAME, String CITYID, String ADDRESS, String LATITUDE, String LONGITUDE);
    Single<DefaultResponce> deleteAddress(String AddressID);
    Single<DefaultResponce> updateAddress(String AddressID, String ADDRESSNAME, String CITYID, String ADDRESS, String LATITUDE, String LONGITUDE);
    Single<User> editProfile(HashMap<String, String> map);
    Single<CategoryResponse>getCategories();
    Single<Product>getProducts(String category_id);
    Single<SliderResponce>getSlider();
    Single<Product>getAllProducts();
    Single<Size>getAllSizes();
    Single<DefaultResponce>setChatNotf(String nodeChild, String message,String node_name);
    Single<DefaultResponce> placeOrder(String address_id, String comment, SendPaymentRequest sendPaymentRequest, String payment_method);
    Single<MyOrder>getAllOrders();
    Single<DefaultResponce>changeStatus();
    Single<DefaultResponce>finish_order(String OrderID);
    Single<MyNotification>getNotf();



















}

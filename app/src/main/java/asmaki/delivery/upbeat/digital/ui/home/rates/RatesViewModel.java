package asmaki.delivery.upbeat.digital.ui.home.rates;

import asmaki.delivery.upbeat.digital.data.DataManager;
import asmaki.delivery.upbeat.digital.data.model.ApiRoot;
import asmaki.delivery.upbeat.digital.ui.base.BaseViewModel;
import asmaki.delivery.upbeat.digital.utils.AppConstants;
import asmaki.delivery.upbeat.digital.utils.ResourceProvider;
import asmaki.delivery.upbeat.digital.utils.rx.SchedulerProvider;

import static asmaki.delivery.upbeat.digital.utils.LogUtils.LOGD;
import static asmaki.delivery.upbeat.digital.utils.LogUtils.LOGE;

public class RatesViewModel  extends BaseViewModel<RatesNavigator> {
    public RatesViewModel(DataManager dataManager, SchedulerProvider schedulerProvider, ResourceProvider mResourceProvider) {
        super(dataManager, schedulerProvider, mResourceProvider);
       // getMyOrders();
    }


    public void getMyOrders() {


        getCompositeDisposable().add(getDataManager().getAllOrders()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .doOnSubscribe(s -> setIsLoading(true))
                .doAfterTerminate(() -> setIsLoading(false))
                .subscribe(
                        myOrder -> {
                            if (myOrder.getmStatusCode().equals(String.valueOf(ApiRoot.SUCCESS))) {
                                getNavigator().getMyOrders(myOrder.getData());
                                LOGD("SUCCESS");
                            } else {
                                LOGE("ERROR");
                                getNavigator().showAlertDialog(myOrder.getmMessage(), AppConstants.ERROR_MESSAGE);
                            }
                        }, throwable -> {
                            LOGE("ERROR:" + throwable.getMessage());
                            getNavigator().showAlertDialog(throwable.getMessage(), AppConstants.NOCONNECTION);

                        }
                ));
        //    }
    }

    String getLanguage(){

        return getDataManager().getLang();
    }
}

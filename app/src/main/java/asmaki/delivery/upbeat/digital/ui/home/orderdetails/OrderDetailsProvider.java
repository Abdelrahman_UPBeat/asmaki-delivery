package asmaki.delivery.upbeat.digital.ui.home.orderdetails;


import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class OrderDetailsProvider {

    @ContributesAndroidInjector
    abstract OrderDetailsFragment provideMainActivityFactory();
}

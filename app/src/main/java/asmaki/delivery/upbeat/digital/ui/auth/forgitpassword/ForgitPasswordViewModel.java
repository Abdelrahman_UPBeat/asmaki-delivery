package asmaki.delivery.upbeat.digital.ui.auth.forgitpassword;

import android.widget.EditText;

import asmaki.delivery.upbeat.digital.data.DataManager;
import asmaki.delivery.upbeat.digital.data.model.ApiRoot;
import asmaki.delivery.upbeat.digital.ui.base.BaseViewModel;
import asmaki.delivery.upbeat.digital.utils.AppConstants;
import asmaki.delivery.upbeat.digital.utils.ResourceProvider;
import asmaki.delivery.upbeat.digital.utils.rx.SchedulerProvider;

import static asmaki.delivery.upbeat.digital.utils.LogUtils.LOGD;
import static asmaki.delivery.upbeat.digital.utils.LogUtils.LOGE;

public class ForgitPasswordViewModel extends BaseViewModel<ForgitNavigator> {
    public ForgitPasswordViewModel(DataManager dataManager, SchedulerProvider schedulerProvider, ResourceProvider mResourceProvider) {
        super(dataManager, schedulerProvider, mResourceProvider);
    }


    public void signIn(EditText email) {
        getNavigator().doHideKeyboard();
        //  if (isValidFields(email, et_pwd)) {
        //if is valid data call api
        getCompositeDisposable().add(getDataManager().forgetPassword(email.getText().toString())
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .doOnSubscribe(s -> setIsLoading(true))
                .doAfterTerminate(() -> setIsLoading(false))
                .subscribe(
                        defaultResponce -> {
                            if (defaultResponce.getmStatusCode().equals(String.valueOf(ApiRoot.SUCCESS))) {
                                LOGD("SUCCESS");
                                // save user token
                             //   getNavigator().goToHome();
                                getNavigator().showAlertDialog(defaultResponce.getmMessage(), AppConstants.FORGET_PWD);

                            } else {
                                LOGE("ERROR");
                                getNavigator().showAlertDialog(defaultResponce.getmMessage(), AppConstants.ERROR_MESSAGE);
                            }
                        }, throwable -> {
                            LOGE("ERROR:" + throwable.getMessage());
                            getNavigator().showAlertDialog(throwable.getMessage(), AppConstants.NOCONNECTION);

                        }
                ));
        //    }
    }

    String getLanguage(){

        return getDataManager().getLang();
    }
}

package asmaki.delivery.upbeat.digital.di.module;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import asmaki.delivery.upbeat.digital.BuildConfig;
import asmaki.delivery.upbeat.digital.constants.ViewModelProviderFactory;
import asmaki.delivery.upbeat.digital.data.AppDataManager;
import asmaki.delivery.upbeat.digital.data.DataManager;
import asmaki.delivery.upbeat.digital.data.local.prefs.AppPreferencesHelper;
import asmaki.delivery.upbeat.digital.data.local.prefs.PreferencesHelper;
import asmaki.delivery.upbeat.digital.data.remote.ApiHelper;
import asmaki.delivery.upbeat.digital.data.remote.AppApiHelper;
import asmaki.delivery.upbeat.digital.data.remote.retrofitrepo.ApiConstants;
import asmaki.delivery.upbeat.digital.data.remote.retrofitrepo.ApiInterceptor;
import asmaki.delivery.upbeat.digital.data.remote.retrofitrepo.ApiRequests;
import asmaki.delivery.upbeat.digital.di.PreferenceInfo;
import asmaki.delivery.upbeat.digital.utils.AppConstants;
import asmaki.delivery.upbeat.digital.utils.ResourceProvider;
import asmaki.delivery.upbeat.digital.utils.rx.AppSchedulerProvider;
import asmaki.delivery.upbeat.digital.utils.rx.SchedulerProvider;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class AppModule {
    @Provides
    @Singleton
    Context provideContext(Application application) {
        return application;
    }

    @Provides
    @PreferenceInfo
    String providePreferenceName() {
        return AppConstants.PREF_NAME;
    }

    @Provides
    @Singleton
    DataManager provideDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    @Singleton
    ApiHelper provideApiHelper(AppApiHelper appApiHelper) {
        return appApiHelper;
    }

    @Provides
    @Singleton
    PreferencesHelper providePreferencesHelper(AppPreferencesHelper appPreferencesHelper) {
        return appPreferencesHelper;
    }

    @Provides
    @Singleton
    ResourceProvider provideResourceProvider(Context context) {
        return new ResourceProvider(context);
    }

    @Provides
    @Singleton
    ViewModelProviderFactory provideViewModelProviderFactory(DataManager dataManager, SchedulerProvider schedulerProvider, ResourceProvider resourceProvider) {
        return new ViewModelProviderFactory(dataManager, schedulerProvider, resourceProvider);
    }

    @Provides
    @Singleton
    ApiRequests getApiInterface(Retrofit retroFit) {
        return retroFit.create(ApiRequests.class);
    }

    @Provides
    @Singleton
    Retrofit getRetrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    OkHttpClient getOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor, ApiInterceptor apiInterceptor) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(apiInterceptor);
        builder.connectTimeout(ApiConstants.CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(ApiConstants.WRITE_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(ApiConstants.READ_TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(httpLoggingInterceptor)
                .build();
        return builder.build();
    }

    @Provides
    @Singleton
    HttpLoggingInterceptor getHttpLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        return httpLoggingInterceptor;
    }

    @Provides
    @Singleton
    ApiInterceptor getApiInterceptor(PreferencesHelper preferencesHelper) {
        return new ApiInterceptor(preferencesHelper);
    }
}

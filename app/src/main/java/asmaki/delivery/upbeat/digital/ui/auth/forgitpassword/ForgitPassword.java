package asmaki.delivery.upbeat.digital.ui.auth.forgitpassword;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.lifecycle.ViewModelProviders;

import javax.inject.Inject;

import asmaki.delivery.upbeat.digital.R;
import asmaki.delivery.upbeat.digital.constants.ViewModelProviderFactory;
import asmaki.delivery.upbeat.digital.databinding.ActivityForgitPasswordBinding;
import asmaki.delivery.upbeat.digital.ui.base.BaseActivity;
import asmaki.delivery.upbeat.digital.utils.CommonUtils;

public class ForgitPassword extends BaseActivity<ActivityForgitPasswordBinding, ForgitPasswordViewModel> implements ForgitNavigator {


    @Inject
    ViewModelProviderFactory factory;
    private ForgitPasswordViewModel forgitPasswordViewModel;
    private ActivityForgitPasswordBinding mBinding;

    @Override
    public int getBindingVariable() {
        return asmaki.delivery.upbeat.digital.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_forgit_password;
    }

    @Override
    public ForgitPasswordViewModel getViewModel() {

        forgitPasswordViewModel = ViewModelProviders.of(this, factory).get(ForgitPasswordViewModel.class);
        return forgitPasswordViewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = getViewDataBinding();
        forgitPasswordViewModel.setNavigator(this);
      //  StatusBarUtil.setTransparent(this);

        Log.d("langsdsd",forgitPasswordViewModel.getLanguage());


        if(forgitPasswordViewModel.getLanguage().equals("ar")){

            mBinding.backbt.setRotation(360);
        }


        mBinding.login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!mBinding.email.getText().toString().equals("")) {
                    forgitPasswordViewModel.signIn(mBinding.email);
                    mBinding.loader.setVisibility(View.VISIBLE);
                }else{
                    mBinding.email.setError(getResources().getString(R.string.empty_email));
                }
            }
        });

        mBinding.backbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                finish();
            }
        });

    }

    @Override
    public void doHideKeyboard() {

    }

    @Override
    public void showAlertDialog(String message, String message_type) {

    //    Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
        CommonUtils.mainAlert(ForgitPassword.this,message_type,message);
        mBinding.loader.setVisibility(View.GONE);

    }
}

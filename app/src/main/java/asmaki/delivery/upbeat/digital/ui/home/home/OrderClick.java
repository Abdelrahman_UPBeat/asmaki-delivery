package asmaki.delivery.upbeat.digital.ui.home.home;

import asmaki.delivery.upbeat.digital.data.model.MyOrder;

public interface OrderClick {

    void getSelectedOrder(MyOrder.Data myorder);

}

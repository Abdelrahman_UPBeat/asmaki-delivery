package asmaki.delivery.upbeat.digital.ui.splash;


import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class SplashProvider {


    @ContributesAndroidInjector
    abstract Splash provideSplashFactory();
}

package asmaki.delivery.upbeat.digital.ui.home.profile;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;

import javax.inject.Inject;

import asmaki.delivery.upbeat.digital.R;
import asmaki.delivery.upbeat.digital.constants.ViewModelProviderFactory;
import asmaki.delivery.upbeat.digital.databinding.FragmentHomeBinding;
import asmaki.delivery.upbeat.digital.databinding.FragmentProfileBinding;
import asmaki.delivery.upbeat.digital.ui.auth.login.LoginActivity;
import asmaki.delivery.upbeat.digital.ui.base.BaseFragment;
import asmaki.delivery.upbeat.digital.ui.base.BaseNavigator;
import asmaki.delivery.upbeat.digital.ui.home.home.HomeViewModel;
import asmaki.delivery.upbeat.digital.ui.home.main.MainActivity;


public class ProfileFragment extends BaseFragment<FragmentProfileBinding, ProfileViewModel> implements ProfileNavigator{

    @Inject
    ViewModelProviderFactory factory;
    private ProfileViewModel profileViewModel;
    private FragmentProfileBinding mBinding;


    @Override
    public int getBindingVariable() {
        return asmaki.delivery.upbeat.digital.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_profile;
    }

    @Override
    public ProfileViewModel getViewModel() {
        profileViewModel = ViewModelProviders.of(this, factory).get(ProfileViewModel.class);
        return profileViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding = getViewDataBinding();
        profileViewModel.setNavigator(this);

        RelativeLayout progressBar = (RelativeLayout) getActivity().findViewById(R.id.helperLin);
        progressBar.setVisibility(View.VISIBLE);


        mBinding.delName.setText(profileViewModel.getMyName());
        mBinding.edEmail.setText(profileViewModel.getMyEmail());
        mBinding.edPass.setText(profileViewModel.getMyPassword());
        mBinding.edPhone.setText(profileViewModel.getMyPhone());

        String myImgaeUrl = profileViewModel.getPhoto();

        if(!myImgaeUrl.equals("")&&!myImgaeUrl.equals(null)&&!myImgaeUrl.isEmpty()&&!myImgaeUrl.equals("null")) {
            Glide.with(getContext()).load(myImgaeUrl).into(mBinding.myImage);
        }

        mBinding.logOutIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                profileViewModel.logOutApi();
                mBinding.loader.setVisibility(View.VISIBLE);

            }
        });

        mBinding.edPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(getActivity(), R.id.nav_host_fragment).navigate(R.id.ChangePassword);

            }
        });

        mBinding.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(getActivity(), R.id.nav_host_fragment).navigate(R.id.UpdateProfile);

            }
        });

        mBinding.switch1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().onBackPressed();

            }
        });

        ImageView toHome = (ImageView) getActivity().findViewById(R.id.toHome);
        toHome.setImageResource(R.drawable.homet);
        ImageView toprofile = (ImageView) getActivity().findViewById(R.id.toprofile);
        toprofile.setImageResource(R.drawable.user);
        ImageView tochat = (ImageView) getActivity().findViewById(R.id.tochat);
        tochat.setImageResource(R.drawable.chat);
        ImageView toRATE = (ImageView) getActivity().findViewById(R.id.toRATE);
        toRATE.setImageResource(R.drawable.star);

        if(profileViewModel.getLanguage().equals("ar")){

            mBinding.switch1.setRotation(0);
        }

    }

    @Override
    public void showAlertDialog(String message, String message_type) {

    }

    @Override
    public void toLogIn() {
        Intent intent =new Intent(getContext(), LoginActivity.class);
        startActivity(intent);
        getActivity().finish();
    }
}

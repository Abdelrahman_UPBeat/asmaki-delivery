package asmaki.delivery.upbeat.digital.ui.splash;

public interface SplashNavigator {

    void goToSelectLang();

    void goToLogin();

    void goToHome();

    void goToIntro();
}

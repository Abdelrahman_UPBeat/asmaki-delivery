package asmaki.delivery.upbeat.digital.ui.auth.login;

import asmaki.delivery.upbeat.digital.ui.base.BaseNavigator;

public interface LoginNavigator extends BaseNavigator.UIChanges, BaseNavigator.ShowAlert {

    void goToHome();

}

package asmaki.delivery.upbeat.digital.ui.home.rates;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import javax.inject.Inject;

import asmaki.delivery.upbeat.digital.R;
import asmaki.delivery.upbeat.digital.constants.ViewModelProviderFactory;
import asmaki.delivery.upbeat.digital.data.model.MyOrder;
import asmaki.delivery.upbeat.digital.databinding.FragmentHomeBinding;
import asmaki.delivery.upbeat.digital.databinding.FragmentRatesBinding;
import asmaki.delivery.upbeat.digital.ui.adapter.OrdersAdapter;
import asmaki.delivery.upbeat.digital.ui.adapter.OrdersRateAdapter;
import asmaki.delivery.upbeat.digital.ui.base.BaseFragment;
import asmaki.delivery.upbeat.digital.ui.home.home.HomeViewModel;
import asmaki.delivery.upbeat.digital.utils.CommonUtils;


public class RatesFragment extends BaseFragment<FragmentRatesBinding, RatesViewModel> implements RatesNavigator {

    @Inject
    ViewModelProviderFactory factory;
    private RatesViewModel ratesViewModel;
    private FragmentRatesBinding mBinding;
    OrdersRateAdapter ordersRateAdapter;

    ArrayList<MyOrder.Data> SelectedOrders = new ArrayList<>();
    boolean done = false;


    @Override
    public int getBindingVariable() {
        return asmaki.delivery.upbeat.digital.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_rates;
    }

    @Override
    public RatesViewModel getViewModel() {

        ratesViewModel = ViewModelProviders.of(this, factory).get(RatesViewModel.class);
        return ratesViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding = getViewDataBinding();
        ratesViewModel.setNavigator(this);
        ratesViewModel.getMyOrders();

        RelativeLayout progressBar = (RelativeLayout) getActivity().findViewById(R.id.helperLin);
        progressBar.setVisibility(View.VISIBLE);
        mBinding.loader.setVisibility(View.VISIBLE);

        mBinding.backbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().onBackPressed();

            }
        });

        ImageView toHome = (ImageView) getActivity().findViewById(R.id.toHome);
        toHome.setImageResource(R.drawable.homet);
        ImageView toprofile = (ImageView) getActivity().findViewById(R.id.toprofile);
        toprofile.setImageResource(R.drawable.usert);
        ImageView tochat = (ImageView) getActivity().findViewById(R.id.tochat);
        tochat.setImageResource(R.drawable.chat);
        ImageView toRATE = (ImageView) getActivity().findViewById(R.id.toRATE);
        toRATE.setImageResource(R.drawable.stars);


        if(ratesViewModel.getLanguage().equals("ar")){

            mBinding.backbt.setRotation(0);
        }


    }


    @Override
    public void getMyOrders(ArrayList<MyOrder.Data> myOrders) {
        mBinding.loader.setVisibility(View.GONE);

        done = false;
        for (int i = 0; i < myOrders.size(); i++) {

            try {
                if (myOrders.get(i).getDelivery_rate() != null)
                    SelectedOrders.add(myOrders.get(i));
            }catch (Exception e){}
        }

        generateCharities(SelectedOrders);

    }

    @Override
    public void showAlertDialog(String message, String message_type) {
        CommonUtils.mainAlert(getActivity(), message_type, message);
        mBinding.loader.setVisibility(View.GONE);

    }


    private void generateCharities(ArrayList<MyOrder.Data> myOrders) {


        if (getContext() != null)
            ordersRateAdapter = new OrdersRateAdapter(getContext(), myOrders);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());

        mBinding.resOrders.setLayoutManager(layoutManager);

        mBinding.resOrders.setAdapter(ordersRateAdapter);
        mBinding.loader.setVisibility(View.GONE);

    }

    @Override
    public void onResume() {
        super.onResume();


        if (done == true) {
            ratesViewModel.getMyOrders();
        }
    }
}

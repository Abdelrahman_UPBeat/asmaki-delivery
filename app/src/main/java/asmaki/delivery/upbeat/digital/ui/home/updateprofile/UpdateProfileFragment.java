package asmaki.delivery.upbeat.digital.ui.home.updateprofile;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import asmaki.delivery.upbeat.digital.R;
import asmaki.delivery.upbeat.digital.constants.ViewModelProviderFactory;
import asmaki.delivery.upbeat.digital.databinding.FragmentUpdateProfileBinding;
import asmaki.delivery.upbeat.digital.ui.base.BaseFragment;
import asmaki.delivery.upbeat.digital.ui.base.BaseNavigator;
import asmaki.delivery.upbeat.digital.utils.CommonUtils;

import static android.app.Activity.RESULT_OK;



public class UpdateProfileFragment extends BaseFragment<FragmentUpdateProfileBinding, UpdateProfileViewModel> implements BaseNavigator.ShowAlert {


    @Inject
    ViewModelProviderFactory factory;
    private UpdateProfileViewModel updateProfileViewModel;
    private FragmentUpdateProfileBinding mBinding;

    boolean photo = false, choose = false, photochoosen = false;
    ;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private int PICK_IMAGE_REQUEST = 1;
    ImageView reportImage;
    private Uri filePath;
    Bitmap bitmap1;

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 7;


    @Override
    public int getBindingVariable() {
        return asmaki.delivery.upbeat.digital.BR.viewModel;

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_update_profile;
    }

    @Override
    public UpdateProfileViewModel getViewModel() {
        updateProfileViewModel = ViewModelProviders.of(this, factory).get(UpdateProfileViewModel.class);
        return updateProfileViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding = getViewDataBinding();
        updateProfileViewModel.setNavigator(this);

        RelativeLayout progressBar = (RelativeLayout) getActivity().findViewById(R.id.helperLin);
        progressBar.setVisibility(View.VISIBLE);

        checkAndroidVersion();

        Log.d("USERRid",updateProfileViewModel.getID());


        if(updateProfileViewModel.getLanguage().equals("ar")){

            mBinding.switch1.setRotation(0);
        }

        mBinding.switch1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().onBackPressed();

            }
        });




        try {
            String Names[] = updateProfileViewModel.getMyName().split(" ");
            mBinding.edFname.setText(Names[0]);
            mBinding.edLname.setText(Names[1]);
        } catch (Exception e) {
            mBinding.edFname.setText(updateProfileViewModel.getMyName());
        }

        mBinding.edPhone.setText(updateProfileViewModel.getMyPhone());
        mBinding.edEmail.setText(updateProfileViewModel.getMyEmail());

        String myImgaeUrl = updateProfileViewModel.getMyImage();
        if (!myImgaeUrl.equals("") && !myImgaeUrl.equals(null) && !myImgaeUrl.isEmpty() && !myImgaeUrl.equals("null")) {
            Glide.with(getContext()).load(myImgaeUrl).into(mBinding.myImage);
        }



        mBinding.btCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                open_dialog();
            }
        });

        mBinding.btSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String FName = mBinding.edFname.getText().toString();
                String LName = mBinding.edLname.getText().toString();
                String Phone = mBinding.edPhone.getText().toString();
                String Email = mBinding.edEmail.getText().toString();


                if (FName.equals("")) {

                    mBinding.edFname.setError(getResources().getString(R.string.empty_field));

                } else if (LName.equals("")) {

                    mBinding.edLname.setError(getResources().getString(R.string.empty_field));


                } else if (Phone.equals("")) {

                    mBinding.edPhone.setError(getResources().getString(R.string.empty_field));


                } else if (Email.equals("")) {

                    mBinding.edEmail.setError(getResources().getString(R.string.empty_field));

                } else {


                    if(photochoosen == true){

                        Log.d("USERRid",updateProfileViewModel.getID());

                        updateProfileViewModel.editProfile(updateProfileViewModel.getID(),FName,LName,Email,Phone,getStringImage(bitmap1));

                    }else{

                        Log.d("USERRid",updateProfileViewModel.getID());


                        updateProfileViewModel.editProfile(updateProfileViewModel.getID(),FName,LName,Email,Phone);

                    }

                }

            }
        });



    }


    @Override
    public void showAlertDialog(String message, String message_type) {

        CommonUtils.mainAlert(getActivity(),message_type,message);

    }

    private void dispatchTakePictureIntent() {
        photo = false;
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private void showFileChooser() {
        photo = true;
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    void open_dialog() {


        final Dialog dialog = new Dialog(getContext());
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        double hi = WindowManager.LayoutParams.MATCH_PARENT / 2;
        lp.height = 400;


        dialog.setContentView(R.layout.camera_browse_dialog);
        dialog.setTitle("");

        Button camera = (Button) dialog.findViewById(R.id.camera);
        Button browse = (Button) dialog.findViewById(R.id.browse);

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dispatchTakePictureIntent();

                photochoosen = true;
                dialog.dismiss();
            }
        });

        browse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showFileChooser();

                dialog.dismiss();
                photochoosen = true;


            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (photo == true) {

            if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

                filePath = data.getData();

                try {


                    bitmap1 = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), filePath);
                    mBinding.myImage.setImageBitmap(bitmap1);


                } catch (IOException e) {
                    e.printStackTrace();
                }

                photochoosen = true;
            }

        } else {
            if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");


                bitmap1 = (Bitmap) extras.get("data");
                mBinding.myImage.setImageBitmap(imageBitmap);

                photochoosen = true;

            }


        }

    }

    private void checkAndroidVersion() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkAndRequestPermissions();

        } else {
            // code for lollipop and pre-lollipop devices
        }

    }

    private boolean checkAndRequestPermissions() {
        int camera = ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA);
        int wtite = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (wtite != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (read != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.d("in fragment on request", "Permission callback called-------");
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("in fragment on request", "CAMERA & WRITE_EXTERNAL_STORAGE READ_EXTERNAL_STORAGE permission granted");
                        // process the normal flow
                        //else any one or both the permissions are not granted
                    } else {
                        Log.d("in fragment on request", "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            showDialogOK("Camera and Storage Permission required for this app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkAndRequestPermissions();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            Toast.makeText(getActivity(), "Go to settings and enable permissions", Toast.LENGTH_LONG)
                                    .show();
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }

    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
}
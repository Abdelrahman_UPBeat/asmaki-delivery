package asmaki.delivery.upbeat.digital.data.remote;

import android.content.Context;

import java.util.HashMap;

import javax.inject.Inject;

import asmaki.delivery.upbeat.digital.data.model.AboutResponce;
import asmaki.delivery.upbeat.digital.data.model.Addresses;
import asmaki.delivery.upbeat.digital.data.model.CallUsResponce;
import asmaki.delivery.upbeat.digital.data.model.CategoryResponse;
import asmaki.delivery.upbeat.digital.data.model.City;
import asmaki.delivery.upbeat.digital.data.model.DefaultResponce;
import asmaki.delivery.upbeat.digital.data.model.LogoutResponce;
import asmaki.delivery.upbeat.digital.data.model.MyNotification;
import asmaki.delivery.upbeat.digital.data.model.MyOrder;
import asmaki.delivery.upbeat.digital.data.model.Product;
import asmaki.delivery.upbeat.digital.data.model.Rate;
import asmaki.delivery.upbeat.digital.data.model.SendPaymentRequest;
import asmaki.delivery.upbeat.digital.data.model.Size;
import asmaki.delivery.upbeat.digital.data.model.SliderResponce;
import asmaki.delivery.upbeat.digital.data.model.TermsResponce;
import asmaki.delivery.upbeat.digital.data.model.User;
import asmaki.delivery.upbeat.digital.data.remote.retrofitrepo.ApiRequests;

import io.reactivex.Single;

public class AppApiHelper implements ApiHelper {


    private final static String TAG = AppApiHelper.class.getSimpleName();
    @Inject
    Context context;
    @Inject
    ApiRequests apiRequests;


    @Inject
    public AppApiHelper() {

    }


    @Override
    public Single<User> register(String first_name, String last_name, String email, String password, String confirm_password, String phone) {
        return apiRequests.register(first_name, last_name, email, password, confirm_password, phone);
    }

    @Override
    public Single<User> Login(String email, String password,String firebase_token) {
        return apiRequests.Login( email,  password,firebase_token);
    }

    @Override
    public Single<DefaultResponce> forgetPassword(String email) {
        return apiRequests.forgetPassword(email);
    }

    @Override
    public Single<City> getCities() {
        return apiRequests.getCities();
    }

    @Override
    public Single<AboutResponce> getAbout() {
        return apiRequests.getAbout();
    }

    @Override
    public Single<TermsResponce> getPrivacy() {
        return apiRequests.getPrivacy();
    }

    @Override
    public Single<Rate> setRateAPI(String rate) {
        return apiRequests.setRateAPI(rate);
    }

    @Override
    public Single<CallUsResponce> callUS(String name, String email, String phone, String message) {
        return apiRequests.callUS(name, email, phone, message);
    }

    @Override
    public Single<LogoutResponce> logOutApi() {
        return apiRequests.logOutApi();
    }

    @Override
    public Single<DefaultResponce> changePassword(String oldPassword, String newPassword, String confirmPassword) {
        return apiRequests.changePassword(oldPassword, newPassword, confirmPassword);
    }

    @Override
    public Single<Addresses> getMyAddress() {
        return apiRequests.getMyAddress();
    }

    @Override
    public Single<DefaultResponce> addAddress(String ADDRESSNAME, String CITYID, String ADDRESS, String LATITUDE, String LONGITUDE) {
        return apiRequests.addAddress(ADDRESSNAME, CITYID, ADDRESS, LATITUDE, LONGITUDE);
    }

    @Override
    public Single<DefaultResponce> deleteAddress(String AddressID) {
        return apiRequests.deleteAddress(AddressID);
    }

    @Override
    public Single<DefaultResponce> updateAddress(String AddressID, String ADDRESSNAME, String CITYID, String ADDRESS, String LATITUDE, String LONGITUDE) {
        return apiRequests.updateAddress(AddressID, ADDRESSNAME, CITYID, ADDRESS, LATITUDE, LONGITUDE);
    }



    @Override
    public Single<User> editProfile(HashMap<String, String> map) {
        return apiRequests.editProfile(map);
    }

    @Override
    public Single<CategoryResponse> getCategories() {
        return apiRequests.getCategories();
    }

    @Override
    public Single<Product> getProducts(String category_id) {
        return apiRequests.getProducts(category_id);
    }

    @Override
    public Single<SliderResponce> getSlider() {
        return apiRequests.getSlider();
    }

    @Override
    public Single<Product> getAllProducts() {
        return apiRequests.getAllProducts();
    }

    @Override
    public Single<Size> getAllSizes() {
        return apiRequests.getAllSizes();
    }

    @Override
    public Single<DefaultResponce> setChatNotf(String nodeChild, String message,String node_name) {
        return apiRequests.setChatNotf(nodeChild, message, node_name);
    }

    @Override
    public Single<DefaultResponce> placeOrder(String address_id, String comment, SendPaymentRequest sendPaymentRequest, String payment_method) {
        return apiRequests.placeOrder(address_id, comment, sendPaymentRequest, payment_method);
    }

    @Override
    public Single<MyOrder> getAllOrders() {
        return apiRequests.getAllOrders();
    }

    @Override
    public Single<DefaultResponce> changeStatus() {
        return apiRequests.changeStatus();
    }

    @Override
    public Single<DefaultResponce> finish_order(String OrderID) {
        return apiRequests.finish_order(OrderID);
    }

    @Override
    public Single<MyNotification> getNotf() {
        return apiRequests.getNotf();
    }


}

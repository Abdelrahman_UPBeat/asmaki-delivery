package asmaki.delivery.upbeat.digital.ui.home.chat;

import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;
import java.util.Map;

import asmaki.delivery.upbeat.digital.data.DataManager;
import asmaki.delivery.upbeat.digital.ui.base.BaseNavigator;
import asmaki.delivery.upbeat.digital.ui.base.BaseViewModel;
import asmaki.delivery.upbeat.digital.utils.ResourceProvider;
import asmaki.delivery.upbeat.digital.utils.rx.SchedulerProvider;

import static asmaki.delivery.upbeat.digital.utils.LogUtils.LOGD;
import static asmaki.delivery.upbeat.digital.utils.LogUtils.LOGE;

public class ChatViewModel  extends BaseViewModel<BaseNavigator.ShowAlert> {
    public ChatViewModel(DataManager dataManager, SchedulerProvider schedulerProvider, ResourceProvider mResourceProvider) {
        super(dataManager, schedulerProvider, mResourceProvider);
    }


    public void sendMessage(String Message){

        String UserID = getDataManager().getUserId();

        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference().child("Delivery/"+UserID);
        Map<String, Object> map = new HashMap<>();



        map.put("id", UserID);
        map.put("Message", Message);
        map.put("sender", "mobile");
        map.put("CreatedTime", ServerValue.TIMESTAMP);
        rootRef.push().setValue(map);

    }


    public String getOurLang(){

        return getDataManager().getLang();
    }

    public void AutoRepleyLoged(String Message){

        String UserID = getDataManager().getUserId();

        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference().child("Delivery/"+UserID);
        Map<String, Object> map = new HashMap<>();



        map.put("id", UserID);
        map.put("Message", Message);
        map.put("sender", "web");
        map.put("CreatedTime", ServerValue.TIMESTAMP);
        rootRef.push().setValue(map);

    }





//    public HashMap<String, Object> getTimestampCreated(){
//            return timestampCreated;
//    }

    String getUserID(){

        return  getDataManager().getUserId();
    }

    String getUserImage(){

        return  getDataManager().getImage();
    }

    public boolean checkLoged(){
        if (getDataManager().getCurrentUserLoggedInMode() == DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_IN.getType()) {

            return true;
        }
        else{

            return false;
        }

    }

    public void setChatNotf(String messages) {
        String UserID=getDataManager().getUserId();
        getCompositeDisposable().add(getDataManager().setChatNotf(UserID,messages,"Delivery")
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .doOnSubscribe(s -> setIsLoading(true))
                .doAfterTerminate(() -> setIsLoading(false))
                .subscribe(
                        defaultDataObj-> {
                            if (defaultDataObj.getmSuccess()) {
                                LOGD("SUCCESS");
                                Log.d("testtttttt",defaultDataObj.getmMessage());

                            } else {
                                LOGE("ERROR");
                                Log.d("testtttttt",defaultDataObj.getmMessage());

                            }
                        }, throwable -> {
                            LOGE("ERROR:" + throwable.getMessage());
                            Log.d("testtttttt",throwable.getMessage());

                        }
                ));
    }

    String getLanguage(){

        return getDataManager().getLang();
    }
}


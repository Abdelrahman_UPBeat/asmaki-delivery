package asmaki.delivery.upbeat.digital.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class CartList implements Parcelable {


    public CartList(){}

    protected CartList(Parcel in) {
    }

    public static final Creator<CartList> CREATOR = new Creator<CartList>() {
        @Override
        public CartList createFromParcel(Parcel in) {
            return new CartList(in);
        }

        @Override
        public CartList[] newArray(int size) {
            return new CartList[size];
        }
    };

    public ArrayList<CartObject> getCarts() {
        return carts;
    }

    public void setCarts(ArrayList<CartObject> carts) {
        this.carts = carts;
    }

    public ArrayList<CartObject>carts;

    public MyOrder.Data getMyOrder() {
        return myOrder;
    }

    public void setMyOrder(MyOrder.Data myOrder) {
        this.myOrder = myOrder;
    }

    public MyOrder.Data myOrder;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
    }
}

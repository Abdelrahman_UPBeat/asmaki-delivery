package asmaki.delivery.upbeat.digital.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;


import asmaki.delivery.upbeat.digital.R;
import asmaki.delivery.upbeat.digital.data.model.MyNotification;
import asmaki.delivery.upbeat.digital.ui.home.notification.NotifClick;
import io.reactivex.annotations.NonNull;

public class NotfAdapter extends RecyclerView.Adapter<NotfAdapter.ViewHolder> {

    private LayoutInflater mInflater;

    ArrayList<MyNotification.Data> Notifications;
    Context context;
    private NotfAdapter adapter;
    NotifClick notifClick;


    public NotfAdapter(Context context, ArrayList<MyNotification.Data> Notifications, NotifClick notifClick) {


        if (context != null) {
            this.mInflater = LayoutInflater.from(context);
            this.Notifications = Notifications;
            this.context = context;
            this.notifClick = notifClick;
            this.adapter = this;
        }

    }


    @Override
    public NotfAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notif, parent, false);


        return new NotfAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull NotfAdapter.ViewHolder holder, int position) {

        holder.Notiftext.setText(Notifications.get(position).getMessage());



        holder.addressLin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notifClick.getNotif(Notifications.get(position));
            }
        });


    }


    @Override
    public int getItemCount() {
        return Notifications.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView Notiftext;
        LinearLayout addressLin;


        public ViewHolder(View itemView) {
            super(itemView);

            Notiftext  = (TextView) itemView.findViewById(R.id.Notiftext);
            addressLin = (LinearLayout)itemView.findViewById(R.id.addressLin);



        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


}

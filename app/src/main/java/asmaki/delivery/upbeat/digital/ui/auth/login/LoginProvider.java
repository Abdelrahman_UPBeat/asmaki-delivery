package asmaki.delivery.upbeat.digital.ui.auth.login;


import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class LoginProvider {

    @ContributesAndroidInjector
    abstract LoginActivity provideLoginActivityFactory();
}

package asmaki.delivery.upbeat.digital.ui.home.chat;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ChatProvider {


    @ContributesAndroidInjector
    abstract ChatFragment provideChatFragmentFactory();
}

package asmaki.delivery.upbeat.digital.ui.splash;

import java.util.Locale;

import asmaki.delivery.upbeat.digital.data.DataManager;
import asmaki.delivery.upbeat.digital.ui.base.BaseViewModel;
import asmaki.delivery.upbeat.digital.utils.ResourceProvider;
import asmaki.delivery.upbeat.digital.utils.rx.SchedulerProvider;

public class SplashViewModel extends BaseViewModel<SplashNavigator>{
public SplashViewModel(DataManager dataManager, SchedulerProvider schedulerProvider, ResourceProvider mResourceProvider) {
    super(dataManager, schedulerProvider, mResourceProvider);
}


public void whichActivityShouldGo() {


    if (getDataManager().getOnBoardingVisible()) {
        if (getDataManager().getIsLangSelected()) {

            if (getDataManager().getCurrentUserLoggedInMode() == DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT.getType()) {
                getNavigator().goToLogin();
            } else {
                getNavigator().goToHome();
            }
        } else {
            String lang = Locale.getDefault().getDisplayLanguage();
            if(lang.equals("English")){
                getDataManager().setLang("en");
            }else{
                getDataManager().setLang("ar");

            }
            getDataManager().setIsLangSelected(true);
            getNavigator().goToSelectLang();

        }
    } else {

        String lang = Locale.getDefault().getDisplayLanguage();
        if(lang.equals("English")){
            getDataManager().setLang("en");
        }else{
            getDataManager().setLang("ar");

        }


        getDataManager().setIsLangSelected(true);
        getDataManager().setOnBoardingVisible(true);
        getNavigator().goToIntro();
    }

}
}

package asmaki.delivery.upbeat.digital.ui.home.profile;

import asmaki.delivery.upbeat.digital.ui.base.BaseNavigator;

public interface ProfileNavigator  extends BaseNavigator.ShowAlert{

    void toLogIn();
}

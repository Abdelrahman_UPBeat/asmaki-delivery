package asmaki.delivery.upbeat.digital.ui.home.main;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;


@Module
public abstract class MainProvider {

    @ContributesAndroidInjector
    abstract MainActivity provideMainActivityFactory();

}

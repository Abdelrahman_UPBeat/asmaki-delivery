package asmaki.delivery.upbeat.digital.ui.home.home;

import java.util.ArrayList;

import asmaki.delivery.upbeat.digital.data.model.MyOrder;
import asmaki.delivery.upbeat.digital.ui.base.BaseNavigator;

public interface HomeNavigator extends BaseNavigator.ShowAlert {


    void getMyOrders(ArrayList<MyOrder.Data> myOrders);
    void toLogIn();

}

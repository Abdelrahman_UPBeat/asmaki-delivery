package asmaki.delivery.upbeat.digital.ui.home.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import android.os.Bundle;
import android.view.View;

import javax.inject.Inject;

import asmaki.delivery.upbeat.digital.R;
import asmaki.delivery.upbeat.digital.constants.ViewModelProviderFactory;
import asmaki.delivery.upbeat.digital.databinding.ActivityLoginBinding;
import asmaki.delivery.upbeat.digital.databinding.ActivityMainBinding;
import asmaki.delivery.upbeat.digital.ui.auth.login.LoginViewModel;
import asmaki.delivery.upbeat.digital.ui.base.BaseActivity;
import asmaki.delivery.upbeat.digital.ui.base.BaseNavigator;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class MainActivity extends BaseActivity<ActivityMainBinding, MainViewModel> implements  BaseNavigator.ShowAlert, HasSupportFragmentInjector{

    @Inject
    ViewModelProviderFactory factory;
    private MainViewModel mainViewModel;
    private ActivityMainBinding mBinding;

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;

    @Override
    public int getBindingVariable() {
        return asmaki.delivery.upbeat.digital.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public MainViewModel getViewModel() {
        mainViewModel = ViewModelProviders.of(this, factory).get(MainViewModel.class);
        return mainViewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = getViewDataBinding();
        mainViewModel.setNavigator(this);

        mBinding.toHomeLin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mBinding.toHome.setImageResource(R.drawable.home);
                mBinding.toprofile.setImageResource(R.drawable.userh);
                mBinding.tochat.setImageResource(R.drawable.chat);
                mBinding.toRATE.setImageResource(R.drawable.star);


                Navigation.findNavController(MainActivity.this, R.id.nav_host_fragment).navigate(R.id.homeFragment);


            }
        });

        mBinding.toCatLin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mBinding.toHome.setImageResource(R.drawable.homet);
                mBinding.toprofile.setImageResource(R.drawable.user);
                mBinding.tochat.setImageResource(R.drawable.chat);
                mBinding.toRATE.setImageResource(R.drawable.star);

                Navigation.findNavController(MainActivity.this, R.id.nav_host_fragment).navigate(R.id.profileFragment);


            }
        });

        mBinding.tochatLin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mBinding.toHome.setImageResource(R.drawable.homet);
                mBinding.toprofile.setImageResource(R.drawable.userh);
                mBinding.tochat.setImageResource(R.drawable.bubble);
                mBinding.toRATE.setImageResource(R.drawable.star);

                Navigation.findNavController(MainActivity.this, R.id.nav_host_fragment).navigate(R.id.ChatFragment);


            }
        });


        mBinding.toRATELin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mBinding.toHome.setImageResource(R.drawable.homet);
                mBinding.toprofile.setImageResource(R.drawable.userh);
                mBinding.tochat.setImageResource(R.drawable.chat);
                mBinding.toRATE.setImageResource(R.drawable.stars);

                Navigation.findNavController(MainActivity.this, R.id.nav_host_fragment).navigate(R.id.RatesFragment);


            }
        });
    }



    @Override
    public void showAlertDialog(String message, String message_type) {

    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }
}

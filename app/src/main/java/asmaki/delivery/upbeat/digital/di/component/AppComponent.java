package asmaki.delivery.upbeat.digital.di.component;

import android.app.Application;

import javax.inject.Singleton;

import asmaki.delivery.upbeat.digital.constants.AppController;
import asmaki.delivery.upbeat.digital.di.builder.ActivityBuilder;
import asmaki.delivery.upbeat.digital.di.module.AppModule;
import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

/**
 * created by Aya mohamed on 8/2/2018.
 */

@Singleton
@Component(modules = {AndroidInjectionModule.class,
        AppModule.class, ActivityBuilder.class})
public interface AppComponent {

    void inject(AppController app);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }
}

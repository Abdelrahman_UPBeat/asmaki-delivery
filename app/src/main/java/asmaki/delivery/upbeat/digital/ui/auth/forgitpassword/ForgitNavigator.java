package asmaki.delivery.upbeat.digital.ui.auth.forgitpassword;

import asmaki.delivery.upbeat.digital.ui.base.BaseNavigator;

public interface ForgitNavigator extends BaseNavigator.UIChanges, BaseNavigator.ShowAlert {
}

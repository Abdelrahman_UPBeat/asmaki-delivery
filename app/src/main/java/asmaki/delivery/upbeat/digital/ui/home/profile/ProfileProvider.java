package asmaki.delivery.upbeat.digital.ui.home.profile;


import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ProfileProvider {

    @ContributesAndroidInjector
    abstract ProfileFragment provideMainActivityFactory();
}

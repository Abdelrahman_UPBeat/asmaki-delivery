package asmaki.delivery.upbeat.digital.ui.home.home;

import asmaki.delivery.upbeat.digital.data.DataManager;
import asmaki.delivery.upbeat.digital.data.model.ApiRoot;
import asmaki.delivery.upbeat.digital.ui.base.BaseNavigator;
import asmaki.delivery.upbeat.digital.ui.base.BaseViewModel;
import asmaki.delivery.upbeat.digital.utils.AppConstants;
import asmaki.delivery.upbeat.digital.utils.ResourceProvider;
import asmaki.delivery.upbeat.digital.utils.rx.SchedulerProvider;

import static asmaki.delivery.upbeat.digital.utils.LogUtils.LOGD;
import static asmaki.delivery.upbeat.digital.utils.LogUtils.LOGE;

public class HomeViewModel extends BaseViewModel<HomeNavigator> {
    public HomeViewModel(DataManager dataManager, SchedulerProvider schedulerProvider, ResourceProvider mResourceProvider) {
        super(dataManager, schedulerProvider, mResourceProvider);
        getMyOrders();
    }


    String getAvailable(){

        return  getDataManager().getAvailable();
    }


    public void getMyOrders() {


        getCompositeDisposable().add(getDataManager().getAllOrders()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .doOnSubscribe(s -> setIsLoading(true))
                .doAfterTerminate(() -> setIsLoading(false))
                .subscribe(
                        myOrder -> {
                            if (myOrder.getmStatusCode().equals(String.valueOf(ApiRoot.SUCCESS))) {
                                getNavigator().getMyOrders(myOrder.getData());
                                LOGD("SUCCESS");
                            } else {
                                LOGE("ERROR");
                                getNavigator().showAlertDialog(myOrder.getmMessage(), AppConstants.ERROR_MESSAGE);
                            }
                        }, throwable -> {
                            LOGE("ERROR:" + throwable.getMessage());
                            getNavigator().showAlertDialog(throwable.getMessage(), AppConstants.NOCONNECTION);

                        }
                ));
        //    }
    }

    public void logOutApi() {


        getCompositeDisposable().add(getDataManager().logOutApi()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .doOnSubscribe(s -> setIsLoading(true))
                .doAfterTerminate(() -> setIsLoading(false))
                .subscribe(
                        logoutResponce -> {
                            if (logoutResponce.getStatusCode().equals(String.valueOf(ApiRoot.SUCCESS))) {
                                LOGD("SUCCESS");

                                getDataManager().setCurrentUserLoggedInMode(DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT);

                                getNavigator().toLogIn();

                            } else {
                                LOGE("ERROR");
                                getNavigator().showAlertDialog(logoutResponce.getMessage(), AppConstants.ERROR_MESSAGE);
                            }
                        }, throwable -> {
                            LOGE("ERROR:" + throwable.getMessage());
                            getNavigator().showAlertDialog(throwable.getMessage(), AppConstants.NOCONNECTION);

                        }
                ));
        //    }
    }


    public void changeStatus() {


        getCompositeDisposable().add(getDataManager().changeStatus()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .doOnSubscribe(s -> setIsLoading(true))
                .doAfterTerminate(() -> setIsLoading(false))
                .subscribe(
                        defaultResponce -> {
                            if (defaultResponce.getmStatusCode().equals(String.valueOf(ApiRoot.SUCCESS))) {
                                getNavigator().showAlertDialog(defaultResponce.getmMessage(), AppConstants.SUCCESS_MESSAGE);
                                if(getAvailable().equals("Yes")) {
                                    getDataManager().setAvailable("No");

                                }else{
                                    getDataManager().setAvailable("Yes");

                                }
                                LOGD("SUCCESS");
                            } else {
                                LOGE("ERROR");
                                getNavigator().showAlertDialog(defaultResponce.getmMessage(), AppConstants.ERROR_MESSAGE);
                            }
                        }, throwable -> {
                            LOGE("ERROR:" + throwable.getMessage());
                            getNavigator().showAlertDialog(throwable.getMessage(), AppConstants.NOCONNECTION);

                        }
                ));
        //    }
    }

    public void saveLang(String lang){

        getDataManager().setLang(lang);

    }

    Boolean checkLanguage(){

        boolean IsArabic = false;

        String lang = getDataManager().getLang();
        if(lang.equals(AppConstants.AR)){

            IsArabic = true;

        }else{

            IsArabic = false;
        }

        return  IsArabic;
    }

    String getLanguage(){

        return getDataManager().getLang();
    }



}

package asmaki.delivery.upbeat.digital.ui.adapter;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.marlonlom.utilities.timeago.TimeAgo;
import com.github.marlonlom.utilities.timeago.TimeAgoMessages;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

;
import asmaki.delivery.upbeat.digital.R;
import asmaki.delivery.upbeat.digital.data.model.Message;
import io.reactivex.annotations.NonNull;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {

    private LayoutInflater mInflater;

    ArrayList<Message> messages;
    Context context;
    String UserImage;
    String Lang;


    public ChatAdapter(Context context, ArrayList<Message> messages, String UserImage, String Lang) {

        this.mInflater = LayoutInflater.from(context);
        this.messages = messages;
        this.context = context;
        this.UserImage = UserImage;
        this.Lang = Lang;

    }


    @Override
    public ChatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_user, parent, false);


        return new ChatAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatAdapter.ViewHolder holder, int position) {

        holder.user_text.setText(messages.get(position).getMyMessage());
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTimeInMillis(Long.parseLong(messages.get(position).getDate()));
        holder.time_t.setText(calendar.getTime().toString());
//        String now =ServerValue.TIMESTAMP.toString();
//        String sendTime = messages.get(position).getDate();
//        SimpleDateFormat sfd = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
//        sfd.format(new Date(sendTime));
//        holder.time_t.setText(sfd.toString());


        if (messages.get(position).getSender().equals("mobile")) {
            if(Lang.equals("ar")){
                holder.user_Lin.setBackground(context.getResources().getDrawable(R.drawable.textchat_ar));
            }else{
                holder.user_Lin.setBackground(context.getResources().getDrawable(R.drawable.textchat));
            }
        } else {

            if(Lang.equals("ar")){

                holder.user_Lin.setBackground(context.getResources().getDrawable(R.drawable.textchatadmin_ar));

            }else{

                holder.user_Lin.setBackground(context.getResources().getDrawable(R.drawable.textchatadmin));

            }

            holder.ll_item1.setGravity(Gravity.END);
            holder.ll_item2.setGravity(Gravity.END);
            holder.iv_profile_image.setVisibility(View.GONE);
            holder.user_text.setTextColor(context.getResources().getColor(R.color.white));

        }
        // if(!UserImage.equals("")&&!UserImage.equals(null)) {

        Locale LocaleBylanguageTag = null;
        long timeInMillis = System.currentTimeMillis();

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            LocaleBylanguageTag = Locale.forLanguageTag(Lang);
            TimeAgoMessages messagess = new TimeAgoMessages.Builder().withLocale(LocaleBylanguageTag).build();

            String text = TimeAgo.using(Long.parseLong(messages.get(position).getDate()), messagess);
            holder.time_t.setText(text);
        }





        Glide.with(context).load(UserImage).apply(new RequestOptions().placeholder(R.drawable.animate).error(R.drawable.animate)).into(holder.iv_profile_image);
        // }
    }


    @Override
    public int getItemCount() {
        return messages.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView user_text, time_t;
        public ImageView iv_profile_image;
        public LinearLayout user_Lin, ll_item2, ll_item1;


        public ViewHolder(View itemView) {
            super(itemView);

            user_text = (TextView) itemView.findViewById(R.id.user_text);
            user_Lin = (LinearLayout) itemView.findViewById(R.id.user_Lin);
            iv_profile_image = (ImageView) itemView.findViewById(R.id.iv_profile_image);
            ll_item1 = (LinearLayout) itemView.findViewById(R.id.ll_item1);
            ll_item2 = (LinearLayout) itemView.findViewById(R.id.ll_item2);
            time_t = (TextView) itemView.findViewById(R.id.time_t);


        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


}

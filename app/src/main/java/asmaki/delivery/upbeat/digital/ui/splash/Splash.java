package asmaki.delivery.upbeat.digital.ui.splash;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.ViewModelProviders;

import com.google.firebase.messaging.FirebaseMessaging;

import javax.inject.Inject;

import asmaki.delivery.upbeat.digital.ui.auth.login.LoginActivity;
import asmaki.delivery.upbeat.digital.ui.home.main.MainActivity;
import asmaki.delivery.upbeat.digital.R;
import asmaki.delivery.upbeat.digital.constants.ViewModelProviderFactory;
import asmaki.delivery.upbeat.digital.databinding.ActivitySplashBinding;
//import asmaki.delivery.upbeat.digital.ui.auth.login.LoginActivity;
import asmaki.delivery.upbeat.digital.ui.base.BaseActivity;
//import asmaki.delivery.upbeat.digital.ui.home.main.MainActivity;
import pub.devrel.easypermissions.EasyPermissions;

public class Splash extends BaseActivity<ActivitySplashBinding, SplashViewModel> implements  SplashNavigator {

    @Inject
    ViewModelProviderFactory factory;
    private SplashViewModel splashViewModel;
    private ActivitySplashBinding mBinding;

    public static  int SPLASH_DISPLAY_LENGTH = 5000;
    int PERMISSION_ALL = 1;
    String[] PERMISSIONS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.CALL_PHONE
    };


    @Override
    public int getBindingVariable() {
        return asmaki.delivery.upbeat.digital.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    public SplashViewModel getViewModel() {
        splashViewModel = ViewModelProviders.of(this, factory).get(SplashViewModel.class);
        return splashViewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = getViewDataBinding();
        splashViewModel.setNavigator(this);

        FirebaseMessaging.getInstance().setAutoInitEnabled(true);

        setupUI();

    }


    @Override
    public void goToSelectLang() {
        Intent intent= new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
        finish();

        Log.d("langw","select");
    }

    @Override
    public void goToLogin() {
        Intent intent= new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
        finish();
        Log.d("langw","login");
    }

    @Override
    public void goToHome() {
        Intent intent= new Intent(getApplicationContext(), MainActivity.class);
       // Intent intent= new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
        finish();
        Log.d("langw","home");

    }

    @Override
    public void goToIntro() {
        Intent intent= new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
        finish();
        Log.d("langw","intro");

    }

    private void setupUI() {
        if(!EasyPermissions.hasPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }else {
            handler();
        }
    }

    private void handler() {
        new Handler().postDelayed(() -> {
            /* Create an Intent that will start the Intro-Activity. */
            splashViewModel.whichActivityShouldGo();
        }, SPLASH_DISPLAY_LENGTH);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0 && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
            Log.i("onRequestPermissions", "you can call the number by clicking on the button");
        }
        handler();
        return;
    }
}

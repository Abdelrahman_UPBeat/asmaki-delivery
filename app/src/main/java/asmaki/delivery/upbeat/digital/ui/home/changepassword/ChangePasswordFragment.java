package asmaki.delivery.upbeat.digital.ui.home.changepassword;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import javax.inject.Inject;

import asmaki.delivery.upbeat.digital.R;
import asmaki.delivery.upbeat.digital.constants.ViewModelProviderFactory;
import asmaki.delivery.upbeat.digital.databinding.FragmentChangePasswordBinding;
import asmaki.delivery.upbeat.digital.ui.base.BaseFragment;
import asmaki.delivery.upbeat.digital.ui.base.BaseNavigator;
import asmaki.delivery.upbeat.digital.utils.CommonUtils;


public class ChangePasswordFragment extends BaseFragment<FragmentChangePasswordBinding,ChangePasswordViewModel> implements BaseNavigator.ShowAlert {


    @Inject
    ViewModelProviderFactory factory;
    private ChangePasswordViewModel changePasswordViewModel;
    private FragmentChangePasswordBinding mBinding;

    @Override
    public int getBindingVariable() {
        return asmaki.delivery.upbeat.digital.BR.viewModel;

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_change_password;
    }

    @Override
    public ChangePasswordViewModel getViewModel() {
        changePasswordViewModel = ViewModelProviders.of(this, factory).get(ChangePasswordViewModel.class);
        return changePasswordViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding = getViewDataBinding();
        changePasswordViewModel.setNavigator(this);





        mBinding.btUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String OldPassWord     =  mBinding.edOldpassword.getText().toString();
                String NewPassWord     =  mBinding.edNewpassword.getText().toString();
                String ConfirmPassWord =  mBinding.edConfirmnewpassword.getText().toString();


                if(!OldPassWord.equals(changePasswordViewModel.getPassword)){

                    mBinding.edOldpassword.setError(getResources().getString(R.string.old_pass_error));


                }else if(NewPassWord.length()<8){

                    mBinding.edConfirmnewpassword.setError(getResources().getString(R.string.password_lenght));


                }else if(!NewPassWord.equals(ConfirmPassWord)){

                    mBinding.edConfirmnewpassword.setError(getResources().getString(R.string.password_confirm));

                }else{

                    changePasswordViewModel.changePassword(OldPassWord,NewPassWord,ConfirmPassWord);
                }



            }
        });

        mBinding.backbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().onBackPressed();

            }
        });


        if(changePasswordViewModel.getLanguage().equals("ar")){

            mBinding.backbt.setRotation(0);
        }


    }

    @Override
    public void showAlertDialog(String message, String message_type) {
        CommonUtils.mainAlert(getActivity(),message_type,message);
        Navigation.findNavController(getActivity(), R.id.nav_host_fragment).navigate(R.id.homeFragment);

    }
}

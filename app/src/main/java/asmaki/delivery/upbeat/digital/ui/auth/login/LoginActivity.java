package asmaki.delivery.upbeat.digital.ui.auth.login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import javax.inject.Inject;

import asmaki.delivery.upbeat.digital.R;
import asmaki.delivery.upbeat.digital.constants.ViewModelProviderFactory;
import asmaki.delivery.upbeat.digital.databinding.ActivityLoginBinding;
import asmaki.delivery.upbeat.digital.ui.auth.forgitpassword.ForgitPassword;
import asmaki.delivery.upbeat.digital.ui.base.BaseActivity;
import asmaki.delivery.upbeat.digital.ui.home.main.MainActivity;
import asmaki.delivery.upbeat.digital.utils.CommonUtils;

public class LoginActivity extends BaseActivity<ActivityLoginBinding, LoginViewModel> implements  LoginNavigator {


    @Inject
    ViewModelProviderFactory factory;
    private LoginViewModel loginViewModel;
    private ActivityLoginBinding mBinding;


    @Override
    public void goToHome() {
        mBinding.loader.setVisibility(View.GONE);
        Intent MainAct = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(MainAct);
        finish();

    }

    @Override
    public int getBindingVariable() {
        return asmaki.delivery.upbeat.digital.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    public LoginViewModel getViewModel() {
        loginViewModel = ViewModelProviders.of(this, factory).get(LoginViewModel.class);
        return loginViewModel;
    }

    @Override
    public void doHideKeyboard() {

    }

    @Override
    public void showAlertDialog(String message, String message_type) {

        CommonUtils.mainAlert(LoginActivity.this,message_type,message);
        mBinding.loader.setVisibility(View.GONE);


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = getViewDataBinding();
        loginViewModel.setNavigator(this);


        mBinding.edForget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(getApplicationContext(), ForgitPassword.class);
                startActivity(intent);
            }
        });

        mBinding.btLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                boolean valid = loginViewModel.isValidFields(mBinding.edEmail,mBinding.edPassword);
                if(valid == true) {
                    FirebaseInstanceId.getInstance().getInstanceId()
                            .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                                @Override
                                public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                    if (!task.isSuccessful()) {
                                        Log.w("tokenState", "getInstanceId failed", task.getException());
                                        return;
                                    }

                                    String token = task.getResult().getToken();

                                  //  mLoginViewModel.signIn(mBinding.etPhone,mBinding.etPwd,token);
                                    loginViewModel.signIn(mBinding.edEmail, mBinding.edPassword,token);

                                }
                            });

                    mBinding.loader.setVisibility(View.VISIBLE);


                }else{



                }

                // CommonUtils.mainAlert(LoginActivity.this,"success");
            }
        });

    }
}

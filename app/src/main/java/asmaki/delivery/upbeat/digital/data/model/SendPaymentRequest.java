package asmaki.delivery.upbeat.digital.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SendPaymentRequest {


    public ArrayList<OrderCProductQuantity> getOrderCampainQuantity() {
        return orderCampainQuantity;
    }

    public void setOrderCampainQuantity(ArrayList<OrderCProductQuantity> orderCampainQuantity) {
        this.orderCampainQuantity = orderCampainQuantity;
    }

    @SerializedName("products")
    @Expose
    private ArrayList<OrderCProductQuantity> orderCampainQuantity;
}
package asmaki.delivery.upbeat.digital.ui.auth.login;

import android.util.Log;
import android.widget.EditText;

import asmaki.delivery.upbeat.digital.data.DataManager;
import asmaki.delivery.upbeat.digital.data.model.ApiRoot;
import asmaki.delivery.upbeat.digital.ui.base.BaseViewModel;
import asmaki.delivery.upbeat.digital.utils.AppConstants;
import asmaki.delivery.upbeat.digital.utils.ResourceProvider;
import asmaki.delivery.upbeat.digital.utils.rx.SchedulerProvider;

import static asmaki.delivery.upbeat.digital.utils.LogUtils.LOGD;
import static asmaki.delivery.upbeat.digital.utils.LogUtils.LOGE;
import static asmaki.delivery.upbeat.digital.utils.ValidationUtils.emailValidation;
import static asmaki.delivery.upbeat.digital.utils.ValidationUtils.pwdValidation;

public class LoginViewModel extends BaseViewModel<LoginNavigator> {

    final ResourceProvider mResourceProvider;

    public LoginViewModel(DataManager dataManager, SchedulerProvider schedulerProvider, ResourceProvider mResourceProvider) {
        super(dataManager, schedulerProvider, mResourceProvider);
        this.mResourceProvider = mResourceProvider;

    }

    public boolean isValidFields(EditText et_email, EditText et_pwd) {
        if (!emailValidation(mResourceProvider, et_email)) {
            return false;
        }
        if (!pwdValidation(mResourceProvider, et_pwd)) {
            return false;
        }
        return true;
    }


    public void signIn(EditText email, EditText et_pwd,String firebase_token) {
        getNavigator().doHideKeyboard();
      //  if (isValidFields(email, et_pwd)) {
            //if is valid data call api
            getCompositeDisposable().add(getDataManager().Login(email.getText().toString(),et_pwd.getText().toString(),firebase_token)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .doOnSubscribe(s -> setIsLoading(true))
                    .doAfterTerminate(() -> setIsLoading(false))
                    .subscribe(
                            user -> {
                                if (user.getStatusCode().equals(String.valueOf(ApiRoot.SUCCESS))) {
                                    LOGD("SUCCESS");
                                    // save user token
                                    getDataManager().setToken(user.getData().getmToken());
                                    getDataManager().setPhoneNumber(user.getData().getPhone_number());
                                    getDataManager().setName(user.getData().getFirst_name()+ " "+ user.getData().getLast_name());
                                    getDataManager().setImage(user.getData().getImage());
                                    getDataManager().setEmail(user.getData().getEmail());
                                    getDataManager().setUserId(user.getData().getId());
                                    Log.d("USERZEFTID",user.getData().getId());
                                    getDataManager().setValid(user.getData().getValid());
                                    getDataManager().setPwd(et_pwd.getText().toString());
                                    getDataManager().setAvailable(user.getData().getAvailable());
                                    getDataManager().setCurrentUserLoggedInMode(DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_IN);
                                    getNavigator().goToHome();
                                  //  getNavigator().showAlertDialog(user.getMessage(), AppConstants.ERROR_MESSAGE);

                                } else {
                                    LOGE("ERROR");
                                    getNavigator().showAlertDialog(user.getMessage(), AppConstants.ERROR_MESSAGE);
                                }
                            }, throwable -> {
                                LOGE("ERROR:" + throwable.getMessage());
                                getNavigator().showAlertDialog(throwable.getMessage(), AppConstants.NOCONNECTION);

                            }
                    ));
    //    }
    }



}

package asmaki.delivery.upbeat.digital.ui.home.updateprofile;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class UpdateProfileProvider {

    @ContributesAndroidInjector
    abstract UpdateProfileFragment provideUpdateProfileFragmentFactory();
}


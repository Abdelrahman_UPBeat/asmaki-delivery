package asmaki.delivery.upbeat.digital.ui.auth.forgitpassword;


import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ForgitPasswordProvider {

    @ContributesAndroidInjector
    abstract ForgitPassword provideForgitPasswordActivityFactory();
}

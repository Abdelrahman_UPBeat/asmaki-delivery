package asmaki.delivery.upbeat.digital.ui.home.notification;

import java.util.ArrayList;

import asmaki.delivery.upbeat.digital.data.model.MyNotification;
import asmaki.delivery.upbeat.digital.data.model.MyOrder;
import asmaki.delivery.upbeat.digital.ui.base.BaseNavigator;

public interface NotificationNavigator extends BaseNavigator.ShowAlert {

    void getNotification(ArrayList<MyNotification.Data> Noti);

    void getMyOrders(ArrayList<MyOrder.Data> myOrders);

}

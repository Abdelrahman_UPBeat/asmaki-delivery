package asmaki.delivery.upbeat.digital.ui.home.notification;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import javax.inject.Inject;

import asmaki.delivery.upbeat.digital.R;
import asmaki.delivery.upbeat.digital.constants.ViewModelProviderFactory;
import asmaki.delivery.upbeat.digital.data.model.CartList;
import asmaki.delivery.upbeat.digital.data.model.CartObject;
import asmaki.delivery.upbeat.digital.data.model.MyNotification;
import asmaki.delivery.upbeat.digital.data.model.MyOrder;
import asmaki.delivery.upbeat.digital.databinding.FragmentNotificationBinding;
import asmaki.delivery.upbeat.digital.ui.adapter.NotfAdapter;
import asmaki.delivery.upbeat.digital.ui.base.BaseFragment;


public class NotificationFragment extends BaseFragment<FragmentNotificationBinding, NotificationViewModel> implements NotificationNavigator,NotifClick {

    @Inject
    ViewModelProviderFactory factory;
    private NotificationViewModel notificationViewModel;
    private FragmentNotificationBinding mBinding;
    NotfAdapter notfAdapter;

    ArrayList<MyNotification.Data> Noti;

    String OrderID;

    boolean done= false;


    @Override
    public int getBindingVariable() {
         return asmaki.delivery.upbeat.digital.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_notification;
    }

    @Override
    public NotificationViewModel getViewModel() {
        notificationViewModel = ViewModelProviders.of(this, factory).get(NotificationViewModel.class);
        return notificationViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding = getViewDataBinding();
        notificationViewModel.setNavigator(this);




    }

    @Override
    public void getNotification(ArrayList<MyNotification.Data> Noti) {
        generateCharities(Noti);
        this.Noti = Noti;
        done = true;
    }

    @Override
    public void getMyOrders(ArrayList<MyOrder.Data> myOrders) {




    }



    @Override
    public void showAlertDialog(String message, String message_type) {

    }



    private void generateCharities(ArrayList<MyNotification.Data> Noti) {


        if (getContext() != null)
            notfAdapter = new NotfAdapter(getContext(),Noti,this );

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());

        mBinding.resNotf.setLayoutManager(layoutManager);

        mBinding.resNotf.setAdapter(notfAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(done){
            generateCharities(Noti);
        }

    }

    @Override
    public void getNotif(MyNotification.Data notf) {

    }
}

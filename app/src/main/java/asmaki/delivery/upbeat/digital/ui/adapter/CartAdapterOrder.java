package asmaki.delivery.upbeat.digital.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import asmaki.delivery.upbeat.digital.R;
import asmaki.delivery.upbeat.digital.data.model.CartObject;
import asmaki.delivery.upbeat.digital.data.model.MyOrder;
import io.reactivex.annotations.NonNull;

public class CartAdapterOrder extends RecyclerView.Adapter<CartAdapterOrder.ViewHolder> {

    private LayoutInflater mInflater;

    List<MyOrder.Data.items> cartObjects;
    Context context;
    private CartAdapterOrder adapter;


    public CartAdapterOrder(Context context, List<MyOrder.Data.items> cartObjects) {


        if (context != null) {
            this.mInflater = LayoutInflater.from(context);
            this.cartObjects = cartObjects;
            this.context = context;
            this.adapter = this;

        }

    }


    @Override
    public CartAdapterOrder.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart_order, parent, false);


        return new CartAdapterOrder.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CartAdapterOrder.ViewHolder holder, int position) {


        holder.ProdName.setText(cartObjects.get(position).getProduct().getName());
        Double FinalPrice = Double.parseDouble(cartObjects.get(position).getUnit_price()) * Double.parseDouble(cartObjects.get(position).getQuantity());
        holder.prodPrice.setText(FinalPrice.toString());
        try {
            holder.ProductSize.setText(cartObjects.get(position).getProduct().getSizes().get(0).getName());
        }catch (Exception e){}
        try {
        holder.ProductClean.setText(cartObjects.get(position).getProduct().getCleaning().get(0).getName());
        }catch (Exception e){}


        holder.ProductQuant.setText(cartObjects.get(position).getQuantity());


        try{
        Glide.with(context).load(cartObjects.get(position).getProduct().getImage().get(0).getImage()).into(holder.ProductPhoto);
        }catch (Exception e){}




    }


    @Override
    public int getItemCount() {
        return cartObjects.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView ProdName, prodPrice, ProductSize, ProductClean, ProductQuant;
        ImageView ProductPhoto;


        public ViewHolder(View itemView) {
            super(itemView);

            ProdName = (TextView) itemView.findViewById(R.id.ProdName);
            prodPrice = (TextView) itemView.findViewById(R.id.prodPrice);
            ProductSize = (TextView) itemView.findViewById(R.id.ProductSize);
            ProductClean = (TextView) itemView.findViewById(R.id.ProductClean);
            ProductQuant = (TextView) itemView.findViewById(R.id.ProductQuant);

            ProductPhoto = (ImageView) itemView.findViewById(R.id.ProductPhoto);
        ;




        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    Double callculateTotal(List<CartObject> cartObjects) {

        double FTotal = 0;

        for (int i = 0; i < cartObjects.size(); i++) {

            Double PriceOfOne = Double.parseDouble(cartObjects.get(i).getPrice());
            Double CountOfOne = Double.parseDouble(cartObjects.get(i).getCount());


            FTotal = FTotal + (PriceOfOne * CountOfOne);

        }

        return FTotal;
    }
}

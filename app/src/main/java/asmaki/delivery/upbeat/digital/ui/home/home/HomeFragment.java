package asmaki.delivery.upbeat.digital.ui.home.home;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import javax.inject.Inject;

import asmaki.delivery.upbeat.digital.R;
import asmaki.delivery.upbeat.digital.constants.ViewModelProviderFactory;
import asmaki.delivery.upbeat.digital.data.model.MyOrder;
import asmaki.delivery.upbeat.digital.databinding.FragmentHomeBinding;
import asmaki.delivery.upbeat.digital.ui.adapter.OrdersAdapter;
import asmaki.delivery.upbeat.digital.ui.auth.login.LoginActivity;
import asmaki.delivery.upbeat.digital.ui.base.BaseFragment;
import asmaki.delivery.upbeat.digital.ui.base.BaseNavigator;
import asmaki.delivery.upbeat.digital.ui.home.main.MainActivity;
import asmaki.delivery.upbeat.digital.ui.splash.Splash;
import asmaki.delivery.upbeat.digital.utils.AppConstants;
import asmaki.delivery.upbeat.digital.utils.CommonUtils;


public class HomeFragment extends BaseFragment<FragmentHomeBinding, HomeViewModel> implements HomeNavigator, OrderClick {


    @Inject
    ViewModelProviderFactory factory;
    private HomeViewModel homeViewModel;
    private FragmentHomeBinding mBinding;
    OrdersAdapter ordersAdapter;

    ArrayList<MyOrder.Data> CurrentmyOrders = new ArrayList<>();
    ArrayList<MyOrder.Data> Previews = new ArrayList<>();
    boolean done = false;
    boolean current = false;


    @Override
    public int getBindingVariable() {
        return asmaki.delivery.upbeat.digital.BR.viewModel;

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_home;

    }

    @Override
    public HomeViewModel getViewModel() {
        homeViewModel = ViewModelProviders.of(this, factory).get(HomeViewModel.class);
        return homeViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding = getViewDataBinding();
        homeViewModel.setNavigator(this);
        mBinding.loader.setVisibility(View.VISIBLE);

        RelativeLayout progressBar = (RelativeLayout) getActivity().findViewById(R.id.helperLin);
        progressBar.setVisibility(View.VISIBLE);

        mBinding.currentUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                mBinding.currentDown.setBackgroundColor(getResources().getColor(R.color.selct));
                mBinding.prevDown.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

                mBinding.btCur.setTextColor(getResources().getColor(R.color.white));
                mBinding.btPrv.setTextColor(getResources().getColor(R.color.order_not_selected));

                current = true;

                generateCharities(CurrentmyOrders);


            }
        });

        mBinding.prevUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mBinding.currentDown.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                mBinding.prevDown.setBackgroundColor(getResources().getColor(R.color.selct));

                mBinding.btCur.setTextColor(getResources().getColor(R.color.order_not_selected));
                mBinding.btPrv.setTextColor(getResources().getColor(R.color.white));

                current = false;

                generateCharities(Previews);


            }
        });

        mBinding.logOutIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                homeViewModel.logOutApi();
                mBinding.loader.setVisibility(View.VISIBLE);


            }
        });

        if(homeViewModel.getAvailable().equals("Yes")){

            mBinding.switch1.setChecked(true);
        }



        mBinding.switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                homeViewModel.changeStatus();

            }
        });

        ImageView toHome = (ImageView) getActivity().findViewById(R.id.toHome);
        toHome.setImageResource(R.drawable.home);
        ImageView toprofile = (ImageView) getActivity().findViewById(R.id.toprofile);
        toprofile.setImageResource(R.drawable.userh);
        ImageView tochat = (ImageView) getActivity().findViewById(R.id.tochat);
        tochat.setImageResource(R.drawable.chat);
        ImageView toRATE = (ImageView) getActivity().findViewById(R.id.toRATE);
        toRATE.setImageResource(R.drawable.star);



        mBinding.LangIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(homeViewModel.getLanguage().equals("ar")){

                    onEnglishClicked();
                }else{
                    onArabicClicked();
                }




            }
        });

        mBinding.NotfIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Navigation.findNavController(getActivity(), R.id.nav_host_fragment).navigate(R.id.NotificationFragment);

            }
        });

    }

    private void generateCharities(ArrayList<MyOrder.Data> myOrders) {


        if (getContext() != null)
            ordersAdapter = new OrdersAdapter(getContext(), myOrders, this);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());

        mBinding.resOrders.setLayoutManager(layoutManager);

        mBinding.resOrders.setAdapter(ordersAdapter);
    }


    @Override
    public void showAlertDialog(String message, String message_type) {
        CommonUtils.mainAlert(getActivity(), message_type, message);
        mBinding.loader.setVisibility(View.GONE);


    }

    @Override
    public void getMyOrders(ArrayList<MyOrder.Data> myOrders) {

        CurrentmyOrders = new ArrayList<>();
        Previews = new ArrayList<>();
        mBinding.loader.setVisibility(View.GONE);


        for (int i = 0; i < myOrders.size(); i++) {

            if (myOrders.get(i).getStatus().equals("Done")) {

                Previews.add(myOrders.get(i));

            } else {

           //     Previews.add(myOrders.get(i));

                CurrentmyOrders.add(myOrders.get(i));

            }

        }

        generateCharities(CurrentmyOrders);
        done = true;

    }

    @Override
    public void toLogIn() {
        Intent intent =new Intent(getContext(), LoginActivity.class);
        startActivity(intent);
        getActivity().finish();
    }


    @Override
    public void getSelectedOrder(MyOrder.Data myorder) {

        Navigation.findNavController(getActivity(), R.id.nav_host_fragment)
                .navigate(HomeFragmentDirections.actionHomeFragmentToOrderDetails(myorder));

    }

    @Override
    public void onResume() {
        super.onResume();


        if (done == true) {
            mBinding.loader.setVisibility(View.GONE);

            if(current == true){
                generateCharities(CurrentmyOrders);

            }else {

                generateCharities(Previews);

            }
        }
    }

    public void onEnglishClicked(){

     //   isArabic = false;

        homeViewModel.saveLang(AppConstants.EN);

        Intent splash = new Intent(getContext(), Splash.class);
        startActivity(splash);
    }

    public void onArabicClicked(){

      //  isArabic = true;

        homeViewModel.saveLang(AppConstants.AR);

        Intent splash = new Intent(getContext(), Splash.class);
        startActivity(splash);



    }
}

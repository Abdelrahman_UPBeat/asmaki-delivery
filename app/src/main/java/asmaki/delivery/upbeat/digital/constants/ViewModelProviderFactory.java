package asmaki.delivery.upbeat.digital.constants;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import javax.inject.Inject;

import asmaki.delivery.upbeat.digital.data.DataManager;
import asmaki.delivery.upbeat.digital.ui.auth.forgitpassword.ForgitPasswordViewModel;
import asmaki.delivery.upbeat.digital.ui.auth.login.LoginViewModel;
import asmaki.delivery.upbeat.digital.ui.home.changepassword.ChangePasswordViewModel;
import asmaki.delivery.upbeat.digital.ui.home.chat.ChatViewModel;
import asmaki.delivery.upbeat.digital.ui.home.home.HomeViewModel;
import asmaki.delivery.upbeat.digital.ui.home.main.MainViewModel;
import asmaki.delivery.upbeat.digital.ui.home.notification.NotificationViewModel;
import asmaki.delivery.upbeat.digital.ui.home.orderdetails.OrderDetailsViewModel;
import asmaki.delivery.upbeat.digital.ui.home.profile.ProfileViewModel;
import asmaki.delivery.upbeat.digital.ui.home.rates.RatesViewModel;
import asmaki.delivery.upbeat.digital.ui.home.updateprofile.UpdateProfileViewModel;
import asmaki.delivery.upbeat.digital.ui.splash.SplashViewModel;
import asmaki.delivery.upbeat.digital.utils.ResourceProvider;
import asmaki.delivery.upbeat.digital.utils.rx.SchedulerProvider;
//import asmaki.delivery.upbeat.digital.ui.auth.forgetpassword.ForgitPasswordViewModel;
//import asmaki.delivery.upbeat.digital.ui.auth.login.LoginViewModel;
//import asmaki.delivery.upbeat.digital.ui.auth.singup.SignUpViewModel;
//import asmaki.delivery.upbeat.digital.ui.home.MainActivityViewModel;
//import asmaki.delivery.upbeat.digital.ui.home.aboutus.AboutFragmentViewModel;
//import asmaki.delivery.upbeat.digital.ui.home.addaddress.AddAddressViewModel;
//import asmaki.delivery.upbeat.digital.ui.home.address.AddressViewModel;
//import asmaki.delivery.upbeat.digital.ui.home.addresscart.AddressCartViewModel;
//import asmaki.delivery.upbeat.digital.ui.home.callus.CallusViewModel;
//import asmaki.delivery.upbeat.digital.ui.home.cart.CartViewModel;
//import asmaki.delivery.upbeat.digital.ui.home.category.CategoryViewModel;
//import asmaki.delivery.upbeat.digital.ui.home.changepassword.ChangePasswordViewModel;
//import asmaki.delivery.upbeat.digital.ui.home.chat.ChatViewModel;
//import asmaki.delivery.upbeat.digital.ui.home.filterproducts.FilterViewModel;
//import asmaki.delivery.upbeat.digital.ui.home.fullscreen.FullScreanViewModel;
//import asmaki.delivery.upbeat.digital.ui.home.homefragment.HomeFragmentViewModel;
//import asmaki.upbeat.digital.ui.home.language.LanguageViewModel;
//import asmaki.upbeat.digital.ui.home.mazed.MazedViewModel;
//import asmaki.upbeat.digital.ui.home.orderdetails.OrderDetailsViewModel;
//import asmaki.upbeat.digital.ui.home.orders.OrderViewModel;
//import asmaki.upbeat.digital.ui.home.payment.PaymentViewModel;
//import asmaki.upbeat.digital.ui.home.productdetails.ProductDetailsViewModel;
//import asmaki.upbeat.digital.ui.home.products.ProductsViewModel;
//import asmaki.upbeat.digital.ui.home.profile.ProfileViewModel;
//import asmaki.upbeat.digital.ui.home.rate.RateFragmentViewModel;
//import asmaki.upbeat.digital.ui.home.searchfragment.SearchViewModel;
//import asmaki.upbeat.digital.ui.home.terms.TermsAndConditionsViewModel;
//import asmaki.upbeat.digital.ui.home.updateprofile.UpdateProfileViewModel;
//import asmaki.upbeat.digital.ui.splash.SplashViewModel;
//import asmaki.upbeat.digital.utils.ResourceProvider;
//import asmaki.upbeat.digital.utils.rx.SchedulerProvider;

public class ViewModelProviderFactory<V> extends ViewModelProvider.NewInstanceFactory {


    private final DataManager dataManager;
    private final SchedulerProvider schedulerProvider;
    private final ResourceProvider resourceProvider;


    @Inject
    public ViewModelProviderFactory(DataManager dataManager, SchedulerProvider schedulerProvider, ResourceProvider resourceProvider) {
        this.dataManager = dataManager;
        this.schedulerProvider = schedulerProvider;
        this.resourceProvider = resourceProvider;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(SplashViewModel.class)) {
            return (T) new SplashViewModel(dataManager, schedulerProvider, resourceProvider);
        } else if (modelClass.isAssignableFrom(LoginViewModel.class)) {
            return (T) new LoginViewModel(dataManager, schedulerProvider, resourceProvider);
        } else if (modelClass.isAssignableFrom(ForgitPasswordViewModel.class)) {
            return (T) new ForgitPasswordViewModel(dataManager, schedulerProvider, resourceProvider);
        } else if (modelClass.isAssignableFrom(MainViewModel.class)) {
            return (T) new MainViewModel(dataManager, schedulerProvider, resourceProvider);
        } else if (modelClass.isAssignableFrom(HomeViewModel.class)) {
            return (T) new HomeViewModel(dataManager, schedulerProvider, resourceProvider);
        } else if (modelClass.isAssignableFrom(ProfileViewModel.class)) {
            return (T) new ProfileViewModel(dataManager, schedulerProvider, resourceProvider);
        }  else if (modelClass.isAssignableFrom(OrderDetailsViewModel.class)) {
            return (T) new OrderDetailsViewModel(dataManager, schedulerProvider, resourceProvider);
        } else if (modelClass.isAssignableFrom(ChangePasswordViewModel.class)) {
            return (T) new ChangePasswordViewModel(dataManager, schedulerProvider, resourceProvider);
        } else if (modelClass.isAssignableFrom(UpdateProfileViewModel.class)) {
            return (T) new UpdateProfileViewModel(dataManager, schedulerProvider, resourceProvider);
        } else if (modelClass.isAssignableFrom(RatesViewModel.class)) {
            return (T) new RatesViewModel(dataManager, schedulerProvider, resourceProvider);
        } else if (modelClass.isAssignableFrom(ChatViewModel.class)) {
            return (T) new ChatViewModel(dataManager, schedulerProvider, resourceProvider);
        }else if (modelClass.isAssignableFrom(NotificationViewModel.class)) {
            return (T) new NotificationViewModel(dataManager, schedulerProvider, resourceProvider);
        }



        throw new IllegalArgumentException("Unknown class name: " + modelClass.getName());
    }

}

package asmaki.delivery.upbeat.digital.ui.home.updateprofile;

import android.util.Log;

import java.util.HashMap;

import asmaki.delivery.upbeat.digital.data.DataManager;
import asmaki.delivery.upbeat.digital.data.model.ApiRoot;
import asmaki.delivery.upbeat.digital.data.remote.retrofitrepo.ApiConstants;
import asmaki.delivery.upbeat.digital.ui.base.BaseNavigator;
import asmaki.delivery.upbeat.digital.ui.base.BaseViewModel;
import asmaki.delivery.upbeat.digital.utils.AppConstants;
import asmaki.delivery.upbeat.digital.utils.ResourceProvider;
import asmaki.delivery.upbeat.digital.utils.rx.SchedulerProvider;

import static asmaki.delivery.upbeat.digital.utils.LogUtils.LOGD;
import static asmaki.delivery.upbeat.digital.utils.LogUtils.LOGE;

public class UpdateProfileViewModel extends BaseViewModel<BaseNavigator.ShowAlert> {
    public UpdateProfileViewModel(DataManager dataManager, SchedulerProvider schedulerProvider, ResourceProvider mResourceProvider) {
        super(dataManager, schedulerProvider, mResourceProvider);
    }


    String getMyName() {
        return getDataManager().getName();
    }

    String getMyPhone() {
        return getDataManager().getPhoneNumber();
    }

    String getMyEmail() {
        return getDataManager().getEmail();
    }

    String getPassword() {
        return getDataManager().getPwd();
    }

    String getMyImage() {
        return getDataManager().getImage();
    }

    String getID() {
        return getDataManager().getUserId();
    }


    public void editProfile(String Id, String FNAme, String LastName, String Email, String Phone, String Image) {


        HashMap<String, String> map = new HashMap<>();
        map.put(ApiConstants.ID, Id);
        map.put(ApiConstants.FIRST_NAME, FNAme);
        map.put(ApiConstants.LAST_NAME, LastName);
        map.put(ApiConstants.EMAIL, Email);
        map.put(ApiConstants.PHONE, Phone);
        map.put(ApiConstants.IMAGE, Image);


        getCompositeDisposable().add(getDataManager().editProfile(map)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .doOnSubscribe(s -> setIsLoading(true))
                .doAfterTerminate(() -> setIsLoading(false))
                .subscribe(
                        user -> {

                            Log.d("zeftwww", user.getMessage());
                            if (user.getStatusCode().equals(String.valueOf(ApiRoot.SUCCESS))) {
                                LOGD("SUCCESS");


                                getNavigator().showAlertDialog(user.getMessage(), AppConstants.SUCCESS_MESSAGE);
                                getDataManager().setEmail(Email);
                                getDataManager().setName(FNAme + " " + LastName);
                                getDataManager().setPhone(Phone);
                                getDataManager().setImage(user.getData().getImage());


                            } else {
                                LOGE("ERROR");
                                getNavigator().showAlertDialog(user.getMessage(), AppConstants.ERROR_MESSAGE);
                            }
                        }, throwable -> {
                            LOGE("ERROR:" + throwable.getMessage());
                            getNavigator().showAlertDialog(throwable.getMessage(), AppConstants.NOCONNECTION);

                        }
                ));
        //    }
    }

    public void editProfile(String Id, String FNAme, String LastName, String Email, String Phone) {


        HashMap<String, String> map = new HashMap<>();
        map.put(ApiConstants.ID, Id);
        map.put(ApiConstants.FIRST_NAME, FNAme);
        map.put(ApiConstants.LAST_NAME, LastName);
        map.put(ApiConstants.EMAIL, Email);
        map.put(ApiConstants.PHONE, Phone);

        getCompositeDisposable().add(getDataManager().editProfile(map)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .doOnSubscribe(s -> setIsLoading(true))
                .doAfterTerminate(() -> setIsLoading(false))
                .subscribe(
                        user -> {
                            if (user.getStatusCode().equals(String.valueOf(ApiRoot.SUCCESS))) {
                                LOGD("SUCCESS");

                                getNavigator().showAlertDialog(user.getMessage(), AppConstants.SUCCESS_MESSAGE);
                                getDataManager().setEmail(Email);
                                getDataManager().setName(FNAme + " " + LastName);
                                getDataManager().setPhone(Phone);
                                getDataManager().setImage(user.getData().getImage());

                            } else {
                                LOGE("ERROR");
                                getNavigator().showAlertDialog(user.getMessage(), AppConstants.ERROR_MESSAGE);
                            }
                        }, throwable -> {
                            LOGE("ERROR:" + throwable.getMessage());
                            getNavigator().showAlertDialog(throwable.getMessage(), AppConstants.NOCONNECTION);

                        }
                ));
        //    }
    }

    String getLanguage(){

        return getDataManager().getLang();
    }
}

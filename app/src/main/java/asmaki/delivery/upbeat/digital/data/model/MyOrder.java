package asmaki.delivery.upbeat.digital.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MyOrder  implements Parcelable {

    protected MyOrder(Parcel in) {
        mMessage = in.readString();
        mStatusCode = in.readString();
        byte tmpMSuccess = in.readByte();
        mSuccess = tmpMSuccess == 0 ? null : tmpMSuccess == 1;
    }

    public static final Creator<MyOrder> CREATOR = new Creator<MyOrder>() {
        @Override
        public MyOrder createFromParcel(Parcel in) {
            return new MyOrder(in);
        }

        @Override
        public MyOrder[] newArray(int size) {
            return new MyOrder[size];
        }
    };

    public String getmMessage() {
        return mMessage;
    }

    public void setmMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public String getmStatusCode() {
        return mStatusCode;
    }

    public void setmStatusCode(String mStatusCode) {
        this.mStatusCode = mStatusCode;
    }

    public Boolean getmSuccess() {
        return mSuccess;
    }

    public void setmSuccess(Boolean mSuccess) {
        this.mSuccess = mSuccess;
    }

    @Expose
    @SerializedName("message")
    private String mMessage;
    @Expose
    @SerializedName("status_code")
    private String mStatusCode;
    @Expose
    @SerializedName("success")
    private Boolean mSuccess;

    public ArrayList<Data> getData() {
        return data;
    }

    @Expose
    @SerializedName("data")
    private ArrayList<Data> data;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mMessage);
        parcel.writeString(mStatusCode);
        parcel.writeByte((byte) (mSuccess == null ? 0 : mSuccess ? 1 : 2));
    }

    public static class Data implements Parcelable{




        @Expose
        @SerializedName("id")
        private String id;

        @Expose
        @SerializedName("status")
        private String status;

        @Expose
        @SerializedName("delivery_id")
        private String delivery_id;

        public String getDelivery_rate() {
            return delivery_rate;
        }

        public void setDelivery_rate(String delivery_rate) {
            this.delivery_rate = delivery_rate;
        }

        @Expose
        @SerializedName("delivery_rate")
        private String delivery_rate;

        @Expose
        @SerializedName("total_cost")
        private String total_cost;

        @Expose
        @SerializedName("total_quantity")
        private String total_quantity;

        @Expose
        @SerializedName("cost")
        private String cost;

        @Expose
        @SerializedName("shipping_value")
        private String shipping_value;

        protected Data(Parcel in) {
            id = in.readString();
            status = in.readString();
            delivery_id = in.readString();
            total_cost = in.readString();
            total_quantity = in.readString();
            cost = in.readString();
            shipping_value = in.readString();
            created_at = in.readString();
            delivery_rate = in.readString();
        }

        public static final Creator<Data> CREATOR = new Creator<Data>() {
            @Override
            public Data createFromParcel(Parcel in) {
                return new Data(in);
            }

            @Override
            public Data[] newArray(int size) {
                return new Data[size];
            }
        };

        public String getId() {
            return id;
        }

        public String getStatus() {
            return status;
        }

        public String getDelivery_id() {
            return delivery_id;
        }

        public String getTotal_cost() {
            return total_cost;
        }

        public String getTotal_quantity() {
            return total_quantity;
        }

        public String getCost() {
            return cost;
        }

        public String getShipping_value() {
            return shipping_value;
        }

        public String getCreated_at() {
            return created_at;
        }

        public ArrayList<Data.items> getItems() {
            return items;
        }

        @Expose
        @SerializedName("created_at")
        private String created_at;


        @Expose
        @SerializedName("items")
        private ArrayList<items> items;

        public Delivery getDelivery() {
            return delivery;
        }

        @Expose
        @SerializedName("delivery")
        private Delivery delivery;


        public Client getClient() {
            return client;
        }

        @Expose
        @SerializedName("client")
        private Client client;

        public Address getAddress() {
            return address;
        }

        @Expose
        @SerializedName("address")
        private Address address;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(id);
            parcel.writeString(status);
            parcel.writeString(delivery_id);
            parcel.writeString(total_cost);
            parcel.writeString(total_quantity);
            parcel.writeString(cost);
            parcel.writeString(shipping_value);
            parcel.writeString(created_at);
        }


        public static class items implements Parcelable{


            @Expose
            @SerializedName("id")
            private String id;

            @Expose
            @SerializedName("quantity")
            private String quantity;

            @Expose
            @SerializedName("cost")
            private String cost;

            @Expose
            @SerializedName("unit_price")
            private String unit_price;

            @Expose
            @SerializedName("offer")
            private String offer;

            protected items(Parcel in) {
                id = in.readString();
                quantity = in.readString();
                cost = in.readString();
                unit_price = in.readString();
                offer = in.readString();
                product = in.readParcelable(Product.class.getClassLoader());
            }

            public static final Creator<items> CREATOR = new Creator<items>() {
                @Override
                public items createFromParcel(Parcel in) {
                    return new items(in);
                }

                @Override
                public items[] newArray(int size) {
                    return new items[size];
                }
            };

            public String getId() {
                return id;
            }

            public String getQuantity() {
                return quantity;
            }

            public String getCost() {
                return cost;
            }

            public String getUnit_price() {
                return unit_price;
            }

            public String getOffer() {
                return offer;
            }

            public Product.Data getProduct() {
                return product;
            }

            public void setProduct(Product.Data product) {
                this.product = product;
            }

            @Expose
            @SerializedName("product")
            private Product.Data product;


            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel parcel, int i) {
                parcel.writeString(id);
                parcel.writeString(quantity);
                parcel.writeString(cost);
                parcel.writeString(unit_price);
                parcel.writeString(offer);
                parcel.writeParcelable(product, i);
            }
        }


        public static class Delivery implements Parcelable{



            @Expose
            @SerializedName("id")
            private String id;

            @Expose
            @SerializedName("name")
            private String name;

            @Expose
            @SerializedName("last_name")
            private String last_name;

            @Expose
            @SerializedName("email")
            private String email;

            @Expose
            @SerializedName("phone_number")
            private String phone_number;

            @Expose
            @SerializedName("image")
            private String image;

            @Expose
            @SerializedName("valid")
            private String valid;

            protected Delivery(Parcel in) {
                id = in.readString();
                name = in.readString();
                last_name = in.readString();
                email = in.readString();
                phone_number = in.readString();
                image = in.readString();
                valid = in.readString();
                role_id = in.readString();
                rate = in.readString();
            }

            public static final Creator<Delivery> CREATOR = new Creator<Delivery>() {
                @Override
                public Delivery createFromParcel(Parcel in) {
                    return new Delivery(in);
                }

                @Override
                public Delivery[] newArray(int size) {
                    return new Delivery[size];
                }
            };

            public String getId() {
                return id;
            }

            public String getName() {
                return name;
            }

            public String getLast_name() {
                return last_name;
            }

            public String getEmail() {
                return email;
            }

            public String getPhone_number() {
                return phone_number;
            }

            public String getImage() {
                return image;
            }

            public String getValid() {
                return valid;
            }

            public String getRole_id() {
                return role_id;
            }

            public String getRate() {
                return rate;
            }

            @Expose
            @SerializedName("role_id")
            private String role_id;

            @Expose
            @SerializedName("rate")
            private String rate;


            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel parcel, int i) {
                parcel.writeString(id);
                parcel.writeString(name);
                parcel.writeString(last_name);
                parcel.writeString(email);
                parcel.writeString(phone_number);
                parcel.writeString(image);
                parcel.writeString(valid);
                parcel.writeString(role_id);
                parcel.writeString(rate);
            }
        }




        public static class Address implements Parcelable {

            @Expose
            @SerializedName("id")
            private String id;

            @Expose
            @SerializedName("name")
            private String name;

            @Expose
            @SerializedName("city_id")
            private String city_id;

            @Expose
            @SerializedName("address")
            private String address;

            @Expose
            @SerializedName("latitude")
            private String latitude;

            @Expose
            @SerializedName("longitude")
            private String longitude;

            @Expose
            @SerializedName("user_id")
            private String user_id;

            @Expose
            @SerializedName("created_at")
            private String created_at;

            protected Address(Parcel in) {
                id = in.readString();
                name = in.readString();
                city_id = in.readString();
                address = in.readString();
                latitude = in.readString();
                longitude = in.readString();
                user_id = in.readString();
                created_at = in.readString();
                updated_at = in.readString();
                deleted_at = in.readString();
            }

            public static final Creator<Address> CREATOR = new Creator<Address>() {
                @Override
                public Address createFromParcel(Parcel in) {
                    return new Address(in);
                }

                @Override
                public Address[] newArray(int size) {
                    return new Address[size];
                }
            };

            public String getId() {
                return id;
            }

            public String getName() {
                return name;
            }

            public String getCity_id() {
                return city_id;
            }

            public String getAddress() {
                return address;
            }

            public String getLatitude() {
                return latitude;
            }

            public String getLongitude() {
                return longitude;
            }

            public String getUser_id() {
                return user_id;
            }

            public String getCreated_at() {
                return created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public String getDeleted_at() {
                return deleted_at;
            }

            @Expose
            @SerializedName("updated_at")
            private String updated_at;

            @Expose
            @SerializedName("deleted_at")
            private String deleted_at;


            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel parcel, int i) {
                parcel.writeString(id);
                parcel.writeString(name);
                parcel.writeString(city_id);
                parcel.writeString(address);
                parcel.writeString(latitude);
                parcel.writeString(longitude);
                parcel.writeString(user_id);
                parcel.writeString(created_at);
                parcel.writeString(updated_at);
                parcel.writeString(deleted_at);
            }
        }


        public static class Client implements Parcelable{



            @Expose
            @SerializedName("id")
            private String id;

            @Expose
            @SerializedName("name")
            private String name;

            @Expose
            @SerializedName("last_name")
            private String last_name;

            @Expose
            @SerializedName("email")
            private String email;

            @Expose
            @SerializedName("phone_number")
            private String phone_number;

            @Expose
            @SerializedName("image")
            private String image;

            @Expose
            @SerializedName("valid")
            private String valid;

            protected Client(Parcel in) {
                id = in.readString();
                name = in.readString();
                last_name = in.readString();
                email = in.readString();
                phone_number = in.readString();
                image = in.readString();
                valid = in.readString();
                role_id = in.readString();
                rate = in.readString();
                created_at = in.readString();
            }

            public static final Creator<Client> CREATOR = new Creator<Client>() {
                @Override
                public Client createFromParcel(Parcel in) {
                    return new Client(in);
                }

                @Override
                public Client[] newArray(int size) {
                    return new Client[size];
                }
            };

            public String getId() {
                return id;
            }

            public String getName() {
                return name;
            }

            public String getLast_name() {
                return last_name;
            }

            public String getEmail() {
                return email;
            }

            public String getPhone_number() {
                return phone_number;
            }

            public String getImage() {
                return image;
            }

            public String getValid() {
                return valid;
            }

            public String getRole_id() {
                return role_id;
            }

            public String getRate() {
                return rate;
            }

            public String getCreated_at() {
                return created_at;
            }

            @Expose
            @SerializedName("role_id")
            private String role_id;

            @Expose
            @SerializedName("rate")
            private String rate;

            @Expose
            @SerializedName("created_at")
            private String created_at;


            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel parcel, int i) {
                parcel.writeString(id);
                parcel.writeString(name);
                parcel.writeString(last_name);
                parcel.writeString(email);
                parcel.writeString(phone_number);
                parcel.writeString(image);
                parcel.writeString(valid);
                parcel.writeString(role_id);
                parcel.writeString(rate);
                parcel.writeString(created_at);
            }
        }



    }



}

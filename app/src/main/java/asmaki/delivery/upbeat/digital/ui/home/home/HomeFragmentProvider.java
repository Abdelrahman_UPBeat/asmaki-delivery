package asmaki.delivery.upbeat.digital.ui.home.home;

import asmaki.delivery.upbeat.digital.ui.home.main.MainActivity;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;


@Module
public abstract class HomeFragmentProvider {

    @ContributesAndroidInjector
    abstract HomeFragment provideMainActivityFactory();
}

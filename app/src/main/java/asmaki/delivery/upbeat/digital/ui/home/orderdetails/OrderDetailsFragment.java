package asmaki.delivery.upbeat.digital.ui.home.orderdetails;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;

import java.util.List;

import javax.inject.Inject;

import asmaki.delivery.upbeat.digital.MapsActivity;
import asmaki.delivery.upbeat.digital.R;
import asmaki.delivery.upbeat.digital.constants.ViewModelProviderFactory;
import asmaki.delivery.upbeat.digital.data.model.CartObject;
import asmaki.delivery.upbeat.digital.data.model.MyOrder;
import asmaki.delivery.upbeat.digital.databinding.FragmentOrderDetailsBinding;
import asmaki.delivery.upbeat.digital.ui.adapter.CartAdapterOrder;
import asmaki.delivery.upbeat.digital.ui.base.BaseFragment;
import asmaki.delivery.upbeat.digital.ui.base.BaseNavigator;
import asmaki.delivery.upbeat.digital.utils.CommonUtils;


public class OrderDetailsFragment extends BaseFragment<FragmentOrderDetailsBinding, OrderDetailsViewModel> implements BaseNavigator.ShowAlert{

    @Inject
    ViewModelProviderFactory factory;
    private OrderDetailsViewModel orderDetailsViewModel;
    private FragmentOrderDetailsBinding mBinding;

    CartAdapterOrder adapter;
    MyOrder.Data myorder;
    String Latt;String Longt;String address;



    @Override
    public int getBindingVariable() {
        return asmaki.delivery.upbeat.digital.BR.viewModel;

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_order_details;

    }

    @Override
    public OrderDetailsViewModel getViewModel() {
        orderDetailsViewModel = ViewModelProviders.of(this, factory).get(OrderDetailsViewModel.class);
        return orderDetailsViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding = getViewDataBinding();
        orderDetailsViewModel.setNavigator(this);

        RelativeLayout progressBar = (RelativeLayout) getActivity().findViewById(R.id.helperLin);
        progressBar.setVisibility(View.VISIBLE);

        try {


            OrderDetailsFragmentArgs args = OrderDetailsFragmentArgs.fromBundle(requireArguments());
            myorder = args.getOrderDetails();


            mBinding.orderNum.setText(myorder.getId());
            mBinding.orderDate.setText(myorder.getCreated_at());

            mBinding.clientName.setText(myorder.getClient().getName());
            mBinding.clientAddress.setText(myorder.getAddress().getAddress());
            mBinding.clientPhone.setText(myorder.getClient().getPhone_number());
            mBinding.total.setText(myorder.getTotal_cost());
            generateCharities(myorder.getItems());



            Latt    = myorder.getAddress().getLatitude();
            Longt   = myorder.getAddress().getLongitude();
            address = myorder.getAddress().getAddress();


            if(myorder.getStatus().equals("Done")){

                mBinding.switch1.setVisibility(View.GONE);
                mBinding.reteText.setVisibility(View.INVISIBLE);

            }else{

                mBinding.rateLin.setVisibility(View.GONE);
//                CardView.LayoutParams layoutParams = (CardView.LayoutParams)
//                        mBinding.myCard.getLayoutParams();
//                layoutParams.height = 10;

            }





        }catch (Exception e){

        }

        mBinding.toLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent mapIntent = new Intent(getContext(), MapsActivity.class);
                mapIntent.putExtra("lat",Latt);
                mapIntent.putExtra("longt",Longt);
                mapIntent.putExtra("Address",address);
                startActivity(mapIntent);

            }
        });

        mBinding.switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                orderDetailsViewModel.finishOrder(myorder.getId());

            }
        });

        mBinding.backbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().onBackPressed();

            }
        });


        if(orderDetailsViewModel.getLanguage().equals("ar")) {

            mBinding.backbt.setRotation(0);
        }

    }

    @Override
    public void showAlertDialog(String message, String message_type) {
        CommonUtils.mainAlert(getActivity(),message_type,message);

    }

    private void generateCharities(List<MyOrder.Data.items> cartObjects) {


        if (getContext() != null)
            adapter = new CartAdapterOrder(getContext(), cartObjects);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());

        mBinding.resCart.setLayoutManager(layoutManager);

        mBinding.resCart.setAdapter(adapter);

        ViewGroup.LayoutParams params=mBinding.resCart.getLayoutParams();
        params.height= cartObjects.size() *(int) getResources().getDimension(R.dimen._84sdp);
        mBinding.resCart.setLayoutParams(params);
    }
}

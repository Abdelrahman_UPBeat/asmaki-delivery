package asmaki.delivery.upbeat.digital.ui.home.changepassword;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ChangePasswordProvider {

    @ContributesAndroidInjector
    abstract ChangePasswordFragment provideChangePasswordFragmentFactory();
}

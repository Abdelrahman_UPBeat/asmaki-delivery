package asmaki.delivery.upbeat.digital.utils;

import android.content.Context;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.marcinmoskala.videoplayview.VideoPlayView;


/**
 * Created by aya mohamed on 11/12/19.
 */

public final class BindingUtils {

    private BindingUtils() {
        // This class is not publicly instantiable
    }


//    @BindingAdapter({"adapter"})
//    public static void setArtist(RecyclerView recyclerView, ArrayList<Artists.Data> items) {
//        ArtistAdapter adapter = (ArtistAdapter) recyclerView.getAdapter();
//        if (adapter != null) {
//            adapter.clearItems();
//            adapter.addItems(items);
//        }
//    }
//
//    @BindingAdapter({"adapter"})
//    public static void setCats(RecyclerView recyclerView, ArrayList<Category> items) {
//        CatAdapter adapter = (CatAdapter) recyclerView.getAdapter();
//        if (adapter != null) {
//            adapter.clearItems();
//            adapter.addItems(items);
//        }
//    }
//
//    @BindingAdapter({"adapter"})
//    public static void setMain(RecyclerView recyclerView, ArrayList<Videos> items) {
//        MainAdapter adapter = (MainAdapter) recyclerView.getAdapter();
//        if (adapter != null) {
//            adapter.clearItems();
//            adapter.addItems(items);
//        }
//    }
//
//    @BindingAdapter({"adapter"})
//    public static void setMainVideos(RecyclerView recyclerView, ArrayList<Video> items) {
//        MainArtistAdapter adapter = (MainArtistAdapter) recyclerView.getAdapter();
//        if (adapter != null) {
//            adapter.clearItems();
//            adapter.addItems(items);
//        }
//    }
//
//
//    @BindingAdapter({"adapter"})
//    public static void setSlider(RecyclerView recyclerView, ArrayList<Slider> items) {
//        SliderAdapter adapter = (SliderAdapter) recyclerView.getAdapter();
//        if (adapter != null) {
//            adapter.clearItems();
//            adapter.addItems(items);
//        }
//    }
//
//
//    @BindingAdapter({"adapter"})
//    public static void setNavItems(RecyclerView recyclerView, ArrayList<NavItem> items) {
//        NavbarAdapter adapter = (NavbarAdapter) recyclerView.getAdapter();
//        if (adapter != null) {
//            adapter.clearItems();
//            adapter.addItems(items);
//        }
//    }
//
//    @BindingAdapter({"adapter"})
//    public static void setCities(RecyclerView recyclerView, ArrayList<City.Data> items) {
//        CityAdapter adapter = (CityAdapter) recyclerView.getAdapter();
//        if (adapter != null) {
//            adapter.clearItems();
//            adapter.addItems(items);
//        }
//    }
//
//    @BindingAdapter({"adapter"})
//    public static void setCountries(RecyclerView recyclerView, ArrayList<Country.Data> items) {
//        CountryAdapter adapter = (CountryAdapter) recyclerView.getAdapter();
//        if (adapter != null) {
//            adapter.clearItems();
//            adapter.addItems(items);
//        }
//    }

    @BindingAdapter("imageUrl")
    public static void setImageUrl(ImageView imageView, String url) {
        Context context = imageView.getContext();
        Glide.with(context).load(url).into(imageView);
    }

    @BindingAdapter("videoImgUrl")
    public static void setVideoImgUrl(VideoPlayView videoPlayView, String url) {
        Context context = videoPlayView.getContext();
        videoPlayView.getImageView().setScaleType(ImageView.ScaleType.FIT_XY);
        Glide.with(context).load(url).into(videoPlayView.getImageView());
    }



}

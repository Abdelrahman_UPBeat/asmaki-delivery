package asmaki.delivery.upbeat.digital.di.builder;



import asmaki.delivery.upbeat.digital.ui.auth.forgitpassword.ForgitPassword;
import asmaki.delivery.upbeat.digital.ui.auth.login.LoginActivity;
import asmaki.delivery.upbeat.digital.ui.home.changepassword.ChangePasswordProvider;
import asmaki.delivery.upbeat.digital.ui.home.chat.ChatProvider;
import asmaki.delivery.upbeat.digital.ui.home.home.HomeFragmentProvider;
import asmaki.delivery.upbeat.digital.ui.home.main.MainActivity;
import asmaki.delivery.upbeat.digital.ui.home.notification.NotificationProvider;
import asmaki.delivery.upbeat.digital.ui.home.orderdetails.OrderDetailsProvider;
import asmaki.delivery.upbeat.digital.ui.home.profile.ProfileProvider;
import asmaki.delivery.upbeat.digital.ui.home.rates.RatesProvider;
import asmaki.delivery.upbeat.digital.ui.home.updateprofile.UpdateProfileProvider;
import asmaki.delivery.upbeat.digital.ui.splash.Splash;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilder {

//    @ContributesAndroidInjector
//    abstract SignupActivity bindSignup();
//
    @ContributesAndroidInjector
    abstract Splash bindSplash();
//
    @ContributesAndroidInjector
    abstract LoginActivity bindLogin();
//
    @ContributesAndroidInjector
    abstract ForgitPassword bindForgit();

    @ContributesAndroidInjector(modules = {HomeFragmentProvider.class, ProfileProvider.class,
            OrderDetailsProvider.class, ChangePasswordProvider.class,
            UpdateProfileProvider.class, NotificationProvider.class
    , RatesProvider.class, ChatProvider.class})
    abstract MainActivity bindMainActvity();
}

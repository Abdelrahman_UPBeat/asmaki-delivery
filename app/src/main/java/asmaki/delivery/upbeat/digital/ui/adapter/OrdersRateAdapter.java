package asmaki.delivery.upbeat.digital.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import asmaki.delivery.upbeat.digital.R;
import asmaki.delivery.upbeat.digital.data.model.MyOrder;
import asmaki.delivery.upbeat.digital.ui.home.home.OrderClick;
import io.reactivex.annotations.NonNull;

public class OrdersRateAdapter extends RecyclerView.Adapter<OrdersRateAdapter.ViewHolder> {

    private LayoutInflater mInflater;

    ArrayList<MyOrder.Data> orders;
    Context context;
    private OrdersRateAdapter adapter;


    public OrdersRateAdapter(Context context, ArrayList<MyOrder.Data> orders) {


        if (context != null) {
            this.mInflater = LayoutInflater.from(context);
            this.orders = orders;
            this.context = context;
            this.adapter = this;
        }

    }


    @Override
    public OrdersRateAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_old_rate, parent, false);


        return new OrdersRateAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull OrdersRateAdapter.ViewHolder holder, int position) {

        holder.clientName.setText(orders.get(position).getClient().getName());
        holder.orderDate.setText(orders.get(position).getCreated_at());

        try {
            holder.rateBar.setRating(Float.parseFloat(orders.get(position).getDelivery_rate()));
        }catch (Exception e){}





    }


    @Override
    public int getItemCount() {
        return orders.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView orderNum, orderDate, clientName;
        RatingBar rateBar;


        public ViewHolder(View itemView) {
            super(itemView);

            orderDate  = (TextView) itemView.findViewById(R.id.orderDate);
            clientName = (TextView) itemView.findViewById(R.id.clientName);
            rateBar    = (RatingBar)itemView.findViewById(R.id.rateBar);




        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


}

package asmaki.delivery.upbeat.digital.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderCProductQuantity {

    @SerializedName("product_id")
    @Expose
    private String product_id;
    @SerializedName("quantity")
    @Expose
    private String quantity;


    public OrderCProductQuantity(String product_id, String quantity) {
        this.product_id = product_id;
        this.quantity = quantity;

    }

}

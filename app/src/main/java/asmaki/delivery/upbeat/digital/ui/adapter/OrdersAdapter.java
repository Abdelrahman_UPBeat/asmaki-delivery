package asmaki.delivery.upbeat.digital.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import asmaki.delivery.upbeat.digital.R;
import asmaki.delivery.upbeat.digital.data.model.MyOrder;
import asmaki.delivery.upbeat.digital.ui.home.home.OrderClick;
import io.reactivex.annotations.NonNull;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> {

    private LayoutInflater mInflater;

    ArrayList<MyOrder.Data> orders;
    Context context;
    private OrdersAdapter adapter;
    OrderClick orderClick;


    public OrdersAdapter(Context context, ArrayList<MyOrder.Data> orders, OrderClick orderClick) {


        if (context != null) {
            this.mInflater = LayoutInflater.from(context);
            this.orders = orders;
            this.context = context;
            this.adapter = this;
            this.orderClick = orderClick;
        }

    }


    @Override
    public OrdersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order, parent, false);


        return new OrdersAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull OrdersAdapter.ViewHolder holder, int position) {

        holder.orderNum.setText(orders.get(position).getId());
        holder.orderDate.setText(orders.get(position).getCreated_at());

        try {
            String split[] = orders.get(position).getCreated_at().split(" ");
            holder.orderDate.setText(split[0]);


        }catch (Exception e){


        }

        if (orders.get(position).getStatus().equals("New Request")) {

            holder.rateBar.setVisibility(View.INVISIBLE);
            holder.order_image.setImageResource(R.color.order_accept_new);
        }else  if (orders.get(position).getStatus().equals("Accepted")) {

        } else if (orders.get(position).getStatus().equals("Declined")) {



        }

        holder.myCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                orderClick.getSelectedOrder(orders.get(position));
            }
        });


    }


    @Override
    public int getItemCount() {
        return orders.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView orderNum, orderDate, orderState;
        ImageView DeletedIcon;
        LinearLayout addressLin;
        RatingBar rateBar;
        ImageView order_image;
        CardView myCard;


        public ViewHolder(View itemView) {
            super(itemView);

            orderNum = (TextView) itemView.findViewById(R.id.orderNum);
            orderDate = (TextView) itemView.findViewById(R.id.orderDate);

            myCard      = (CardView) itemView.findViewById(R.id.myCard);


        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


}

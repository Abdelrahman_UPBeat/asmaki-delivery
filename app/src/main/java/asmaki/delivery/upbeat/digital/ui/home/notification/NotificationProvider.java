package asmaki.delivery.upbeat.digital.ui.home.notification;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;


@Module
public abstract class NotificationProvider {

    @ContributesAndroidInjector
    abstract NotificationFragment provideMazedFragmentFactory();
}

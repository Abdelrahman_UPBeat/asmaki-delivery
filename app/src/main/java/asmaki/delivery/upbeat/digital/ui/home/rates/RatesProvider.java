package asmaki.delivery.upbeat.digital.ui.home.rates;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class RatesProvider {

    @ContributesAndroidInjector
    abstract RatesFragment provideMainActivityFactory();
}

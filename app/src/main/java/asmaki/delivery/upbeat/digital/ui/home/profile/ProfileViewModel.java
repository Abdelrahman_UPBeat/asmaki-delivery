package asmaki.delivery.upbeat.digital.ui.home.profile;

import asmaki.delivery.upbeat.digital.data.DataManager;
import asmaki.delivery.upbeat.digital.data.model.ApiRoot;
import asmaki.delivery.upbeat.digital.ui.base.BaseNavigator;
import asmaki.delivery.upbeat.digital.ui.base.BaseViewModel;
import asmaki.delivery.upbeat.digital.utils.AppConstants;
import asmaki.delivery.upbeat.digital.utils.ResourceProvider;
import asmaki.delivery.upbeat.digital.utils.rx.SchedulerProvider;

import static asmaki.delivery.upbeat.digital.utils.LogUtils.LOGD;
import static asmaki.delivery.upbeat.digital.utils.LogUtils.LOGE;

public class ProfileViewModel extends BaseViewModel<ProfileNavigator> {
    public ProfileViewModel(DataManager dataManager, SchedulerProvider schedulerProvider, ResourceProvider mResourceProvider) {
        super(dataManager, schedulerProvider, mResourceProvider);
    }


    String getPhoto() {
        return getDataManager().getImage();
    }

    String getMyPhone(){
        return getDataManager().getPhoneNumber();
    }

    String getMyName(){
        return getDataManager().getName();
    }

    String getMyEmail(){
        return getDataManager().getEmail();
    }

    String getMyPassword(){
        return getDataManager().getPwd();
    }

    public void logOutApi() {


        getCompositeDisposable().add(getDataManager().logOutApi()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .doOnSubscribe(s -> setIsLoading(true))
                .doAfterTerminate(() -> setIsLoading(false))
                .subscribe(
                        logoutResponce -> {
                            if (logoutResponce.getStatusCode().equals(String.valueOf(ApiRoot.SUCCESS))) {
                                LOGD("SUCCESS");

                                getDataManager().setCurrentUserLoggedInMode(DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT);

                                getNavigator().toLogIn();

                            } else {
                                LOGE("ERROR");
                                getNavigator().showAlertDialog(logoutResponce.getMessage(), AppConstants.ERROR_MESSAGE);
                            }
                        }, throwable -> {
                            LOGE("ERROR:" + throwable.getMessage());
                            getNavigator().showAlertDialog(throwable.getMessage(), AppConstants.NOCONNECTION);

                        }
                ));
        //    }
    }

    String getLanguage(){

        return getDataManager().getLang();
    }
}

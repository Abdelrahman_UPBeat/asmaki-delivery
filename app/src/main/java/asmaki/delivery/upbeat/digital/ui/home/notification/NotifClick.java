package asmaki.delivery.upbeat.digital.ui.home.notification;


import asmaki.delivery.upbeat.digital.data.model.MyNotification;

public interface NotifClick {

    void getNotif(MyNotification.Data notf);
}
